import sortBy from 'lodash.sortby';
import logger from '../config/logger.js';
import Track from '../models/Track.js';
import { sources } from '../lib/crawlers.js';

const singleTrackAction = (action) => async (req, res) => {
  try {
    Track.novaTubeUserId = req.cookies.novaTubeUserId;
    const track = await Track.findByYoutubeId(req.params.id);
    const updatedTrack = await action(req, track).save();
    res.json(updatedTrack);
  } catch (err) {
    logger.error(err.stack);
    res.sendStatus(500);
  }
};

export const flagTrack = singleTrackAction(
  (req, track) => track.flag(),
);
export const replaceTrack = singleTrackAction(
  (req, track) => track.replace(req.params.newID),
);
export const likeTrack = singleTrackAction(
  (req, track) => track.toggleLike(req.cookies.novaTubeUserId),
);
export const dislikeTrack = singleTrackAction(
  (req, track) => track.toggleDislike(req.cookies.novaTubeUserId),
);

const apiOptions = (query) => ({
  timestamp: query.timestamp && parseInt(query.timestamp, 10),
  sources: query.sources && Object.fromEntries(query.sources.split('-').map((kv) => {
    const [k, v] = kv.split('_');
    return [k, parseInt(v, 10)];
  })),
  chronological: query.chronological !== 'false',
  limit: query.limit && parseInt(query.limit, 10),
});

export const getTracks = async (req, res) => {
  try {
    req.setTimeout(1 * 60 * 1000); // 1 min
    Track.novaTubeUserId = req.cookies.novaTubeUserId;
    await Track.crawl(req.app); // Refetch when querying
    const options = apiOptions(req.query);
    const tracks = req.query.startWith
      ? [await Track.findByYoutubeId(req.query.startWith)]
      : await Track.findApi(options);
    return res.json(
      sortBy(tracks, (t) => (options.chronological ? 1 : -1) * t.lastPlayedAtTimestamp),
    );
  } catch (err) {
    logger.error(err.stack);
    return res.sendStatus(500);
  }
};

export const refresh = async (req, res) => {
  try {
    req.setTimeout(1 * 60 * 1000); // 1 min
    const tracks = await Track.crawl();
    res.json(tracks);
  } catch (err) {
    logger.error(err.stack);
    res.sendStatus(500);
  }
};

export const getSources = (req, res) => {
  res.json(sources.map((crawler) => ({
    key: crawler.key,
    name: crawler.name,
    defaultLevel: crawler.defaultLevel,
  })));
};
