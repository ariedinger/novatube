import path from 'path';
import Track from '../models/Track.js';

export const index = (req, res) => {
  res.sendFile('build/index.html', { root: path.resolve(path.dirname('')) });
};

// export const track = (req, res) => {
//   Track.findByYoutubeId(req.params.id).then((track) => {
//     const markups = render({
//       tracklist: [{
//         ...track,
//         isCurrent: true,
//       }],
//     });
//     res.send(markups);
//   });
// };

export const cover = (req, res) => {
  Track.findByYoutubeId(req.params.id).then((t) => {
    if (t && t.cover) {
      res.redirect(t.coverWithDefault);
    }
  });
};
