import mongoose from 'mongoose';
import logger from './logger.js';
import mongooseFindEach from '../lib/mongoose-find-each.js';

mongoose.Promise = global.Promise;
mongoose.plugin(mongooseFindEach);

const MONGO_URL = 'mongodb://db/novatube';

export const mongooseConnect = () => mongoose.connect(MONGO_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
}).catch((err) => {
  logger.error('MongoDB Connection Error. Please make sure that MongoDB is running.', err.stack);
});

mongoose.set('useFindAndModify', false);

export default mongoose;
