import winston from 'winston';

export default winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
  ].concat(
    new winston.transports.Console({
      format: winston.format.simple(),
    }),
  ),
});
