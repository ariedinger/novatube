import express from 'express';
import path from 'path';

export default express.static(path.join(path.resolve(), 'build'), {
  maxAge: 31557600000,
});
