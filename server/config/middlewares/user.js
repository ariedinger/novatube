import crypto from 'crypto';

const stringNotNull = (str) => str && str !== 'undefined' && str !== 'null';

export default [(req, res, next) => {
  let token;
  if (stringNotNull(req.query.novaTubeUserId)) {
    token = req.query.novaTubeUserId;
  } else if (stringNotNull(req.cookies.novaTubeUserId)) {
    token = req.cookies.novaTubeUserId;
  } else {
    token = crypto.randomBytes(12).toString('hex');
  }
  res.cookie('novaTubeUserId', token, { maxAge: 31557600000, httpOnly: true });
  res.locals.novaTubeUserId = token;

  next();
}, (req, res, next) => {
  res.locals.user = req.user;
  next();
}];
