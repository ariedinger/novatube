import session from 'express-session';
import cookieParser from 'cookie-parser';
import lusca from 'lusca';

const isProduction = process.env.NODE_ENV === 'production';

export default [
  cookieParser(process.env.SESSION_SECRET),
  session({
    resave: false,
    saveUninitialized: true,
    secret: process.env.SESSION_SECRET,
    proxy: isProduction,
    cookie: {
      secure: isProduction,
    },
  }), lusca({
  // csrf: true,
    xframe: 'SAMEORIGIN',
    xssProtection: true,
  })];
