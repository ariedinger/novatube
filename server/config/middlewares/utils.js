export default (req, res, next) => {
  const fullUrl = (path) => {
    const pathWithDefault = path || '/';
    return `${req.headers['x-forwarded-proto'] || req.protocol}://${req.headers.host || req.get('host')}${pathWithDefault[0] === '/' ? '' : '/'}${pathWithDefault}`;
  };
  req.fullUrl = fullUrl;
  res.locals.fullUrl = fullUrl;
  next();
};
