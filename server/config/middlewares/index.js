import requestParser from './requestParser.js';
import session from './session.js';
import staticMiddleware from './static.js';
import user from './user.js';
import utils from './utils.js';

export default []
  .concat(requestParser)
  .concat(session)
  .concat(staticMiddleware)
  .concat(user)
  .concat(utils);
