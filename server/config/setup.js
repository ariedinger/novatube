import express from 'express';
import middlewares from './middlewares/index.js';
import routes from './routes.js';

const app = express();

app.set('port', process.env.PORT || 3000);
app.set('trust proxy', app.get('env') === 'production');

middlewares.forEach((m) => app.use(m));

routes.forEach((route) => {
  app[route.method || 'get'](route.path, route.controller);
});
  
export default app;
