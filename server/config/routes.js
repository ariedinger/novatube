import {
  index,
} from '../controllers/homeController.js';

import {
  getTracks,
  getSources,
  refresh,
  flagTrack,
  likeTrack,
  dislikeTrack,
} from '../controllers/apiController.js';

export default [{
  path: '/',
  controller: index,
}, {
  path: '/about',
  controller: index,
}, {
  path: '/track/:id',
  controller: index,
}, {
  path: '/track/:id/cover',
  controller: index,
}, {
  path: '/api/sources',
  controller: getSources,
}, {
  path: '/api/tracks',
  controller: getTracks,
}, {
  path: '/api/refresh',
  controller: refresh,
}, {
  path: '/api/track/:id/flag',
  method: 'post',
  controller: flagTrack,
}, {
  path: '/api/track/:id/like',
  method: 'post',
  controller: likeTrack,
}, {
  path: '/api/track/:id/dislike',
  method: 'post',
  controller: dislikeTrack,
}];
