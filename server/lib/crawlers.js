import get from 'lodash.get';
import fetch from 'node-fetch';
import cheerio from 'cheerio';

import { daytimeToTimestamp } from '../utils/time.js';
import { smartWordCapitalize } from '../utils/string.js';

const buildCrawler = ({
  key,
  defaultCover,
  method = 'get',
  format = 'html',
  formData = () => ({}),
  extract = (data) => data,
  url,
  debug = false,
}) => {
  const actualUrl = (typeof url === 'function') ? url() : url;
  const request = async () => {
    const result = await fetch(actualUrl, {
      method,
      ...formData(),
    });
    switch (format) {
      case 'html':
        const text = await result.text();
        if (debug) console.log(text);
        return cheerio.load(text);
      case 'json':
        const json = result.json();
        if (debug) console.log(json);
        return json;
      default:
        if (debug) console.log(result);
        return result;
    }
  };

  return async () => extract(await request())
    .map((result) => ({
      ...result,
      source: key,
      cover: result.cover === defaultCover ? null : result.cover,
      artist: get(result, 'artist', '').trim(),
      title: get(result, 'title', '').trim(),
    }))
    .filter((res) => res && res.artist && res.title && res.lastPlayedAtTimestamp && res.source);
};

export const sources = [{
  key: 'nova',
  name: 'Nova',
  website: 'https://www.nova.fr',
  defaultLevel: 100,
  defaultCover: 'https://www.nova.fr/wp-content/themes/lnei-wp-theme-child-nova/dist/images/nova-default_342a3fb2.png',
  url: () => `https://www.nova.fr/wp-admin/admin-ajax.php?action=loadmore_programs&page=1&&time=${(new Date()).getHours()}%3A${(new Date()).getMinutes()}&radio=910&afp_nonce=a24743b15f`,
  minConfidence: 0.75,
  format: 'html',
  extract: ($) => $('.wwtt_content').toArray().map((el) => {
    const artist = smartWordCapitalize($(el).find('.wwtt_right h2').text().trim());
    const title = smartWordCapitalize($(el).find('.wwtt_right p:not(.time)').text().trim());
    const timestamp = daytimeToTimestamp($(el).find('.wwtt_right p.time').text().trim().split(':'));
    const cover = ($(el).find('.img_wwtt img.attachment-cube-thumb').attr('src') || '').trim();
    return {
      title,
      artist,
      cover,
      lastPlayedAtTimestamp: timestamp,
    };
  }),
}, {
  key: 'fip',
  name: 'FIP',
  website: 'https://www.fip.fr',
  defaultLevel: 0,
  defaultCover: 'https://www.fip.fr/sites/all/themes/custom/fip/img/fond_titres_diffuses_degrade.png',
  minConfidence: 0.8,
  url: 'https://www.fip.fr/latest/api/graphql?operationName=History&variables=%7B%22first%22%3A10%2C%22after%22%3A%22%22%2C%22stationId%22%3A7%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22ce6791c62408f27b9338f58c2a4b6fdfd9d1afc992ebae874063f714784d4129%22%7D%7D',
  format: 'json',
  extract: (json) => json.data.timelineCursor.edges.map((track) => ({
    title: track.node.subtitle,
    artist: track.node.title,
    cover: track.node.cover,
    lastPlayedAtTimestamp: track.node.start_time * 1000,
  })),
}, {
  key: 'sympa',
  name: 'Radio Sympa',
  website: 'https://www.radio.net/s/laradiosympa',
  defaultLevel: 0,
  defaultCover: 'https://d3kle7qwymxpcy.cloudfront.net/images/broadcasts/d0/0c/104717/2/c175.png',
  minConfidence: 0.75,
  url: 'https://widgets.mediacpanel.com/widgets/public/tyizqrhg/recentlyplayed.php',
  format: 'html',
  extract: ($) => $('table').toArray().map((el) => {
    const [artist_title, timeinfos] = $(el).find('td:last-child font').html().split('<br>');
    const [artist, title] = artist_title.split('-');
    const timeArr = timeinfos.split('Played at');
    const time = timeArr[timeArr.length - 1];
    const timestamp = daytimeToTimestamp(time.trim().split(':'));
    const cover = ($(el).find('td:first-child img').attr('src') || '').trim();
    return {
      title,
      artist,
      cover,
      lastPlayedAtTimestamp: timestamp,
    };
  }),
  // }, {
  //   key: 'meuh',
  //   name: 'Radio Meuh',
  //   website: 'http://www.radiomeuh.com',
  //   defaultLevel: 0,
  //   defaultCover: 'https://novatu.be/images/meuh.png',
  //   url: 'https://www.radiomeuh.com/player/rtdata/tracks.json',
  //   format: 'json',
  //   minConfidence: 0.8,
  //   extract: json => {
  //     return json.map(track => {
  //       let timestamp = daytimeToTimestamp(track.time.split(':'));
  //       return {
  //         title: track.titre,
  //         artist: track.artist,
  //         cover: track.imgSrc,
  //         lastPlayedAtTimestamp: timestamp,
  //       }
  //     })
  //   }
  // }, {
  //   key: 'skyrock',
  //   name: 'Skyrock',
  //   website: 'https://skyrock.fm',
  //   defaultLevel: 0,
  //   defaultCover: 'https://skyrock.fm/images/iphone/pochette600x600.jpg',
  //   url: 'https://skyrock.fm/api/v3/sound',
  //   format: 'json',
  //   minConfidence: 0.85,
  //   extract: json => {
  //     return json.sounds.map(sound => {
  //       return {
  //         title: sound.title,
  //         artist: sound.artist_display,
  //         cover: sound.cover_uri_130,
  //         lastPlayedAtTimestamp: sound.start_ts * 1000,
  //       };
  //     });
  //   }
  // }, {
  //   key: 'vostok',
  //   name: 'Radio Vostok',
  //   website: 'https://radiovostok.ch',
  //   defaultLevel: 0,
  //   defaultCover: 'https://static.radio.fr/images/broadcasts/00/2c/11205/c300.png',
  //   minConfidence: 0.85,
  //   url: 'https://playlist.radiovostok.ch/modules/trouverunmorceauWORDPRESS.php',
  //   method: 'post',
  //   formData: () => ({
  //     jour: new Date().getDate(),
  //     mois: new Date().getMonth() + 1,
  //     annee: new Date().getFullYear(),
  //     date: '2015-04-22', // WTF
  //     heure: new Date().getHours(),
  //     min: new Date().getMinutes(),
  //   }),
  //   extract: $ => {
  //     return $($('article').toArray().last()).find('p').toArray().map(el => {
  //       const content = $(el).html().trim();
  //       const matches = content.match(/(\d{2}-\d{2}-\d{4} \d{2}:\d{2}:\d{2}) : <strong>(.*) - (.*)<\/strong>/);
  //       if (!matches) return;
  //       const datetime = matches[1];
  //       const artist = matches[2];
  //       const title = matches[3];
  //       const time = moment.parse(datetime, 'DD-MM-YYYY HH:mm:ss');
  //       return {
  //         title: title,
  //         artist: artist,
  //         lastPlayedAtTimestamp: time.unix(),
  //       }
  //     })
  //   }
}];

export default sources.map(buildCrawler);
