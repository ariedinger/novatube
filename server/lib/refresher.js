import get from 'lodash.get';
import Track from '../models/Track.js';
import logger from '../config/logger.js';

const trackLogguer = (tracks) => {
  const sourcedTracks = tracks
    // .filter((track) => track && track.justCreated)
    .reduce((result, track) => ({
      ...result,
      [track.source]: get(result, track.source, []).concat(track.toString()),
    }), {});
  if (sourcedTracks.length) {
    logger.info('Crawled new tracks:', sourcedTracks);
  } else {
    logger.info('No new track this time');
  }
};

const refresher = async (app) => {
  try {
    trackLogguer(await Track.crawl(app));
  } catch (err) {
    logger.error(err.stack);
  }
};

export default (app) => {
  refresher(app);
  setInterval(() => refresher(app), 1000 * 60 * 5); // every 5 minute
};
