import get from 'lodash.get';
import { google } from 'googleapis';
import moment from 'moment';
import levenshtein from 'js-levenshtein';
import { parameterize } from '../utils/string.js';
import { concatMap } from '../utils/array.js';
import { olderThan } from '../utils/time.js';
import { maxBy, first } from '../utils/array.js';

const youtube = google.youtube({
  version: 'v3',
  auth: process.env.YOUTUBE_KEY,
});

const isolateBrackets = (word, baseFactor = 1) => {
  const bracketGroups = word.match(/(\(|\[)(.*?)(\)|\])/g) || [];
  const noBrackets = bracketGroups.reduce((result, parenthese) => result.replace(parenthese, ''), word);
  return bracketGroups.map((bracketString) => ({
    word: bracketString,
    factor: baseFactor * 0.5,
  })).concat({
    word: noBrackets,
    factor: baseFactor * 1,
  });
};

const removeInsignificantWords = (word) => ['feat', 'radio edit', 'edit', 'remix', 'ft', 'the', 'version', 'and'].reduce(
  (result, uselessWord) => result.replace(` ${uselessWord} `, ''),
  word,
).replace(/\s/, ' ').trim();

const splitByArtistOrTitle = (string) => {
  const splittingWords = ['feat', '/', ',', 'ft.', '-'];
  return splittingWords
    .reduce(
      (splitResult, splitWord) => concatMap(splitResult, (subGroup) => subGroup.split(splitWord)),
      [string],
    )
    .map((groupOfWords) => removeInsignificantWords(parameterize(groupOfWords)))
    .filter((groupOfWords) => groupOfWords.length);
};

const calculateScore = (title, obj) => {
  const parameterizedTitle = parameterize(title);
  return {
    ...obj,
    factor: 1
      - obj.factor
      * (
        levenshtein(parameterizedTitle, obj.word)
        - Math.max(parameterizedTitle.length - obj.word.length, 0)
      ) / obj.word.length,
  };
};

const searchMatchFactor = (artist, title, youtubeInfos) => {
  const isolatedGroupsFactors = concatMap(
    isolateBrackets(artist).concat(isolateBrackets(title, 0.75)),
    (obj) => splitByArtistOrTitle(obj.word).map(
      (isolatedWord) => ({ word: isolatedWord, factor: obj.factor }),
    ),
  ).map((obj) => calculateScore(get(youtubeInfos, 'snippet.title', ' '), obj));

  return isolatedGroupsFactors.reduce((result, elem) => (result * elem.factor), 1);
};

const youtubeTitleFactor = (artist, title, youtubeInfos) => {
  const youtubeTitle = parameterize(get(youtubeInfos, 'snippet.title', ' '));
  const query = parameterize(`${artist} ${title}`);
  const lengthDifferenceFactor = 1 - 0.1 * Math.abs(
    query.length - youtubeTitle.length,
  ) / query.length;

  return [{
    word: 'cover',
    factor: 0.2,
  }, {
    word: 'karaoke',
    factor: 0.2,
  }, {
    word: 'live',
    factor: 0.8,
  }, {
    word: 'official',
    factor: 1.1,
  }].reduce(
    (factor, obj) => (factor * (youtubeTitle.indexOf(obj.word) > -1 ? obj.factor : 1)),
    lengthDifferenceFactor,
  );
};

const youtubeViewCountFactor = (youtubeInfos) => {
  if (!olderThan(new Date(get(youtubeInfos, 'snippet.publishedAt')), 60 * 24 * 30)) return 1; // No view count if younger than 1 month
  return Math.min(Math.max((Math.log(parseInt(get(youtubeInfos, 'statistics.viewCount', 1), 10)) / Math.log(10) - 3), 0), 1);
};

const youtubeDurationFactor = (youtubeInfos) => 1 - Math.min(Math.max(moment.duration(get(youtubeInfos, 'contentDetails.duration')).asMinutes() - 15, 0) / 45, 1);

const youtubeSearchIndexFactor = (youtubeInfos) => 1 - 0.05 * youtubeInfos.searchIndex;

const calculateConfidence = (artist, title, youtubeInfos, options) => {
  const optionsWithDefaults = {
    initialFactor: 1,
    details: false,
    ...options,
  };

  const matchFactor = searchMatchFactor(artist, title, youtubeInfos);
  const titleFactor = youtubeTitleFactor(artist, title, youtubeInfos);
  const viewCountFactor = youtubeViewCountFactor(youtubeInfos);
  const durationFactor = youtubeDurationFactor(youtubeInfos);
  const searchIndexFactor = youtubeSearchIndexFactor(youtubeInfos);

  const confidence = Math.min(1, optionsWithDefaults.initialFactor
    * matchFactor
    * titleFactor
    * viewCountFactor
    * durationFactor
    * searchIndexFactor);

  return {
    ...youtubeInfos,
    confidence,
    ...(optionsWithDefaults.details ? {
      factors: {
        match: matchFactor,
        title: titleFactor,
        viewCount: viewCountFactor,
        duration: durationFactor,
        searchIndex: searchIndexFactor,
      },
    } : {}),
  };
};

export const getYoutubeDetails = async (artist, title, videoIds, options) => {
  const res = await youtube.videos.list({
    id: [].concat(videoIds).join(','),
    part: 'id,snippet,statistics,contentDetails,status',
  });
  return get(res, 'data.items', [])
    .map((youtubeResult, searchIndex) => ({ ...youtubeResult, searchIndex }))
    .map((youtubeResult) => calculateConfidence(artist, title, youtubeResult, options));
};

export const searchYoutube = async (artist, title, options) => {
  const res = await youtube.search.list({
    part: 'id',
    // videoCategoryId: '10', // Music
    maxResults: 5,
    q: parameterize(`${artist} ${title}`),
    type: 'video',
    regionCode: 'FR',
    order: 'relevance',
    videoEmbeddable: true,
  });
  const videosIds = get(res, 'data.items', []).map((item) => get(item, 'id.videoId'));
  return await getYoutubeDetails(artist, title, videosIds, options);
};

export const bestYoutubeVideo = async (artist, title) => {
  const youtubeResults = await searchYoutube(artist, title);
  if (!youtubeResults) return;
  return maxBy(youtubeResults, res => res.confidence);
};
