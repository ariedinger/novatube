export default (schema) => {
  schema.statics.findEach = function (criteria, applyFn) {
    return this.find(criteria)
      .then((results) => Promise.all(results.map(applyFn)));
  };
  return schema;
};
