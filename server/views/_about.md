## About this project

I built this app because I wanted to be able to listen to the delicious Nova Playlists while working, without being bothered by radio commercials. It is also a proof of concept of what can be done using various tools and data available on the web.

> How does it work?

The server simply scrapes data on the [C'était quoi ce titre ?](http://www.novaplanet.com/radionova/cetaitquoicetitre) page of Nova Radio Website, then searches over Youtube for the matching track, and generates a playlist out of it.

> Is this illegal?

Using Nova's trademark is not permitted according to their [Terms and Services](http://www.novaplanet.com/mentions-legales). This could be easily removed if required.

Besides, the technics used behind this app are completely legal. Nova doesn't seem to forbid scraping on its website and YouTube videos are accessed through a regular API usage.

The fact that some video should not be on Youtube is nohow a Novatube concern. Fascinating how exhaustive their library is, isn't it?

> How does it compares to listening to Radio Nova?

There are quite some differences. Being positive or negative is highly personal:

- Video along with the music
- Easy track info access
- No commercials
- No comments from Nova
- No upcoming local gigs announcement
- No Nova-specific track versions
- No Engineered sound (track level can vary etc.)

> Can Nova block this app?

Totally, and very easily. They'd simply put their [C'était quoi ce titre ?](http://www.novaplanet.com/radionova/cetaitquoicetitre) page behind a [Captcha](http://en.wikipedia.org/wiki/CAPTCHA) and automatic scrapping is over.

The most important question is: **Should** they block it?

> Should Nova block this app?

As you could expect, with all my subjectivity, my answer is **_no_**. Here's why.

First, as mentionned before, the experience is very different being here than listening to Nova Radio.

Also, there is no need to fear from something that is actually small. Just like advertisers aren't scared of [AdBlock](https://adblockplus.org), because "it represents only a really small percentage of internet traffic".

Besides, putting restrictions on tools that make life simpler is never a good strategy. Arte forbade the [website that allowed to download their replay video](http://floriancrouzat.net/arte/). As a consequence, the code of a [Chrome extension doing the same job is now Open-Source](https://github.com/GuGuss/ARTE-7-Playground). [Same as Youtube and Soundcloud](https://github.com/rg3/youtube-dl/).

But also, nothing proves that this is something bad for the Radio itself. Those who want to listen to music on the internet without commercials already can. There are thousands of tools, apps and websites for having radio-like playlist of many genres on the web.
Among my favorites: [Whyd](http://whyd.com/), [Soundsgood](http://soundsgood.co/), [TrackGuru](http://www.trackguru.co/).
Novatube is just another one.
On the other hand, once offline, it could become natural to set its radio on 101.5 frequency in Paris. Being quoted and hacked around, creating buzz around a brand is always good and free communication/advertising.

Finally, there may be some lessons to take out this. Which brings us to the last question:

> If Nova reads this, what would I say?

Rethink your business model. I am not the only one being a commercial **_hater_**. You could make a subscription-based web access to the radio, which would play some secret hidden tracks during everybody else's commercials. Would be cool, at low price, wouldn't it?

Also, you should gather some data from your listeners. I simply have a "like" button, but it is already a lot. Out of it, I could generate an automated Best-Of every month much like you do with [Nova Tunes](http://www.novaplanet.com/novamag/playlist-playlist-novatunes-29), but automatically.

And having been listening to Nova quite a lot lately, I would also suggest to vary music even more. I know you guys can really dig music pieces of gold, but there are quite some tracks that come over very often - usually not the best ones - it is not very encouraging to being 100% faithful to Nova.

---

As a final word, French-speaking readers should watch this very good video, summarizing what has been said:

