/* eslint-disable func-names */
import get from 'lodash.get';
import jsonSelect from 'mongoose-json-select';

import mongoose from '../config/database.js';
import crawlers, { sources } from '../lib/crawlers.js';
import {
  bestYoutubeVideo,
} from '../lib/smartYoutubeSearch.js';
import logger from '../config/logger.js';
import {
  flatten,
  insertUnique,
  without,
  maxBy,
} from '../utils/array.js';

const trackSchema = new mongoose.Schema({
  source: { type: String, index: true, required: true },
  title: { type: String, required: true },
  artist: { type: String, required: true },
  cover: String,
  youtubeID: { type: String, index: true },
  youtubeRefreshedAt: Date,
  youtubeGivenBySource: { type: Boolean, default: false },
  youtubeTitle: String,
  confidence: { type: Number, index: true },
  lastPlayedAtTimestamp: { type: Number, index: true },
  playedAtTimestamps: Array,
  likeUsers: Array,
  dislikeUsers: Array,
  userSubmissions: Array,
});

trackSchema.plugin(jsonSelect, {
  source: true,
  title: true,
  artist: true,
  cover: false,
  coverWithDefault: true,
  userSubmission: true,
  youtubeID: true,
  youtubeTitle: true,
  confidence: true,
  lastPlayedAtTimestamp: true,
  playCount: true,
  likeCount: true,
  dislikeCount: true,
  isLiked: true,
  isDisliked: true,
  _id: false,
});

trackSchema.set('toJSON', {
  virtuals: true,
});

trackSchema.virtual('isLiked').get(function () {
  return this.likeUsers.includes(this.constructor.novaTubeUserId);
});

trackSchema.virtual('isDisliked').get(function () {
  return this.dislikeUsers.includes(this.constructor.novaTubeUserId);
});

trackSchema.virtual('lastPlayedAt').get(function () {
  return this.timestampToDate();
});

trackSchema.virtual('playCount').get(function () {
  return this.playedAtTimestamps.length;
});

trackSchema.virtual('likeCount').get(function () {
  return this.likeUsers.length;
});

trackSchema.virtual('dislikeCount').get(function () {
  return this.dislikeUsers.length;
});

trackSchema.virtual('coverWithDefault').get(function () {
  return this.cover || get(sources.find(({ key }) => key === this.source), 'defaultCover');
});

trackSchema.virtual('userSubmission').get(function () {
  return maxBy(this.userSubmissions, (s) => s.users.length);
});

trackSchema.pre('save', function (next) {
  this.playedAtTimestamps = insertUnique(this.playedAtTimestamps, this.lastPlayedAtTimestamp);
  next();
});

trackSchema.methods.toString = function () {
  return `${this.title} - ${this.artist} @ ${this.source}`;
};

trackSchema.methods.customToString = function () {
  return `${this.youtubeID}: ${this.title} by ${this.artist}. Scrapped ${this.playCount} times. Last: ${this.lastPlayedAtTimestamp}. Liked ${this.likeCount} times. Disliked ${this.dislikeCount} times.`;
};

trackSchema.methods.timestampToDate = function () {
  return this.lastPlayedAtTimestamp && new Date(parseInt(this.lastPlayedAtTimestamp, 10));
};

trackSchema.methods.toggleLike = function (userID) {
  if (this.isLiked) {
    this.likeUsers = without(this.likeUsers, userID);
  } else {
    this.likeUsers = insertUnique(this.likeUsers, userID);
    this.dislikeUsers = this.dislikeUsers.without(userID);
  }
  return this;
};

trackSchema.methods.toggleDislike = function (userID) {
  if (this.isDisliked) {
    this.dislikeUsers = without(this.dislikeUsers, userID);
  } else {
    this.dislikeUsers = insertUnique(this.dislikeUsers, userID);
    this.likeUsers = this.likeUsers.without(userID);
  }
  return this;
};

trackSchema.methods.saveWithYoutubeResult = function (youtubeResult) {
  return this.set({
    youtubeID: get(youtubeResult, 'id'),
    youtubeRefreshedAt: new Date(),
    youtubeTitle: get(youtubeResult, 'snippet.title'),
    confidence: youtubeResult.confidence,
  }).save();
};

trackSchema.statics.findByYoutubeId = function (youtubeID) {
  return this.findOne({
    youtubeID,
  });
};

trackSchema.methods.handleExistingTrack = function (trackInfos, youtubeResult) {
  return this.set({
    lastPlayedAtTimestamp: Math.max(trackInfos.lastPlayedAtTimestamp, this.lastPlayedAtTimestamp),
    cover: trackInfos.cover || this.cover, // update because becomes quickly outdated
  }).saveWithYoutubeResult(youtubeResult);
};

trackSchema.statics.processIncomingTrackInfos = async function (trackInfos) {
  const {
    artist,
    title,
    source,
    lastPlayedAtTimestamp,
  } = trackInfos;

  const track = await this.findOne({
    youtubeID: { $ne: null },
    source,
    artist,
    title,
    lastPlayedAtTimestamp: {
      $gt: lastPlayedAtTimestamp - (1000 * 15 * 60), // search around 30 minutes
      $lt: lastPlayedAtTimestamp + (1000 * 15 * 60),
    },
  });
  if (track) {
    return track;
  }
  const youtubeResult = await bestYoutubeVideo(artist, title);
  if (!youtubeResult) {
    logger.info(`:( No video Found for ${title} by ${artist} from ${source}`)
    // throw new Error(`:( No video Found for ${title} by ${artist} from ${source}`);
    return;
  }
  const foundTrack = await this.findOne({
    source,
    youtubeID: get(youtubeResult, 'id'),
  }) || await this.findOne({
    source,
    artist,
    title,
  });
  if (foundTrack) {
    return foundTrack.handleExistingTrack(trackInfos, youtubeResult);
  }
  const newTrack = await (new this(trackInfos)).saveWithYoutubeResult(youtubeResult);
  return {
    ...newTrack,
    justCreated: true,
  };
};

const LAST_FETCH_TIMESTAMP = 'LAST_FETCH_TIMESTAMP';

trackSchema.statics.crawl = async function (app) {
  if (app
    && app.get(LAST_FETCH_TIMESTAMP)
    && (Date.now() - app.get(LAST_FETCH_TIMESTAMP) < 1000 * 60 * 5)
  ) {
    logger.info('No need to refresh', app.get(LAST_FETCH_TIMESTAMP));
    return [];
  }
  const results = flatten(
    await Promise.all(crawlers.map(async (crawler) => {
      const tracks = await crawler();
      return tracks.map((t) => this.processIncomingTrackInfos(t));
    })),
  );
  if (app) {
    app.set(LAST_FETCH_TIMESTAMP, Date.now());
  }
  return results;
};

trackSchema.statics.removeYoutubeIds = async function () {
  const result = await this.updateMany({
    youtubeID: { $ne: null },
    youtubeRefreshedAt: {
      $lte: new Date(Date.now() - 1000 * 60 * 60 * 24 * 30), // older than 1 month
    },
  }, {
    youtubeID: null,
    youtubeRefreshedAt: null,
    youtubeTitle: null,
    confidence: null,
  });
  logger.info('YoutubeIds removed', result);
  return result;
};

trackSchema.statics.findApi = function (apiParams) {
  const chronological = apiParams.chronological && apiParams.timestamp;

  const queryParams = {
    chronological,
    interval: chronological
      ? { $gt: apiParams.timestamp }
      : { $lt: apiParams.timestamp || Date.now() },
    limit: apiParams.limit || 25,
    sources: apiParams.sources || {},
  };

  const query = {
    youtubeID: { $ne: null },
    lastPlayedAtTimestamp: queryParams.interval,
    ...(Object.entries(queryParams.sources).length ? {
      $or: Object.entries(queryParams.sources).map(([k, v]) => ({
        source: k,
        $expr: { $lt: [{ $size: '$playedAtTimestamps' }, v] },
      })),
    } : {}),
  };

  return this.find(query)
    .sort(`${(queryParams.chronological) ? '' : '-'}lastPlayedAtTimestamp`)
    .limit(queryParams.limit);
};

export default mongoose.model('Track', trackSchema);
