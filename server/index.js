import app from './config/setup.js';
import logger from './config/logger.js';
import refresher from './lib/refresher.js';
import {mongooseConnect} from './config/database.js';

mongooseConnect().then(() => {
  app.listen(app.get('port'), () => {
    refresher(app);
    logger.info(`NovaTube listening on port ${app.get('port')} in ${app.get('env')} mode`);
  });
}).catch(err => {
  console.error('Could not connect to DB');
  console.error(err);
});

