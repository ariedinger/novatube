'use strict';
import moment from 'moment-timezone';

export const olderThan = (date, minutes) => (new Date()).getTime() - date.getTime() > minutes * 60 * 1000;

export const daytimeToTimestamp = daytimeArray => 
  moment().tz(process.env.TZ).hours(parseInt(daytimeArray[0])).minutes(parseInt(daytimeArray[1])).seconds(parseInt(daytimeArray[2]) || 0).unix() * 1000;