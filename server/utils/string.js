import npmParameterize from 'parameterize';

export const parameterize = (str, delimiter = ' ') => npmParameterize(str, str.len, delimiter);

export const capitalize = string => (string ? string.charAt(0).toUpperCase() + string.slice(1).toLowerCase() : '');

export const smartWordCapitalize = string => {
  const words = string.split(/\s/);
  return words.map(word => {
    return word.length > 1 ? capitalize(word) : word.toLowerCase();
  }).join(' ');
}