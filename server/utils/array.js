export const concatMap = (array, mapFn) => array.reduce(
  (result, element) => result.concat(mapFn(element)),
  [],
);

export const flatten = (array) => array.reduce(
  (a, b) => a.concat(b),
  [],
);

export const first = array => array[0];
export const last = array => array[array.length - 1];

export const maxBy = (array, fn) => {
  return array.slice(1).reduce((result, element) => (fn(element) > fn(result) ? element : result), first(array));
};

export const insertUnique = (array, element) => {
  return array.includes(element) ? array : array.concat(element);
};

export const without = (array, element) => {
  return array.filter(el => el !== element);
}

