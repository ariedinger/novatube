FROM node:latest
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev libkrb5-dev node-gyp
RUN mkdir /novatube
WORKDIR /novatube
COPY package.json /novatube/package.json
RUN yarn install