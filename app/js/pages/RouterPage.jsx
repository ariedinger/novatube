import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import LandingPage from './LandingPage';
import AboutPage from './AboutPage';

export default () => (
  <BrowserRouter>
    <Route exact path="/" component={LandingPage} />
    <Route exact path="/track/:trackid" component={LandingPage} />
    <Route exact path="/about" component={AboutPage} />
  </BrowserRouter>
);
