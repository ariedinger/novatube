import React from 'react';

import Layout from '../components/layout/Layout';
import App from '../components/App';

export default () => (
  <Layout>
    <App />
  </Layout>
);
