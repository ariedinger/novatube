import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import rootReducer from './root-reducer';
import {INITIAL_STATE} from './reducers/options';

const LOCALSTORAGE_KEY = 'NovatubeSources';
const middlewares = process.env.NODE_ENV === 'development' ? [logger] : [];

export default initialState => {
  const persistedState = localStorage.getItem(LOCALSTORAGE_KEY) && JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY)) || INITIAL_STATE;

  const store = createStore(rootReducer, {options: persistedState, ...initialState}, applyMiddleware(...middlewares));

  store.subscribe(()=> {
    const {timestamp, ...savableOptions} = store.getState().options;
    localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(savableOptions))
  })

  return store;
}
