export const SET_SOURCES = 'SET_SOURCES';

export const APPEND_TRACKS = 'APPEND_TRACKS';
export const RESET_TRACKS = 'RESET_TRACKS';

export const PLAY_TRACK = 'PLAY_TRACK';
export const NEXT_TRACK = 'NEXT_TRACK';

export const PAUSE_PLAYER = 'PAUSE_PLAYER';
export const RESUME_PLAYER = 'RESUME_PLAYER';

export const SAVE_OPTIONS = 'SAVE_OPTIONS';

export const setSources = sources => {
  return {
    type: SET_SOURCES,
    payload: {sources},
  }
}

export const playTrack = (track) => {
  return {
    type: PLAY_TRACK,
    payload: {track},
  }
}

export const nextTrack = () => {
  return {
    type: NEXT_TRACK,
  }
}

export const pausePlayer = () => {
  return {
    type: PAUSE_PLAYER,
  }
};

export const resumePlayer = () => {
  return {
    type: RESUME_PLAYER,
  }
};

export const appendTracks = tracks => {
  return {
    type: APPEND_TRACKS,
    payload: {tracks},
  }
}

export const resetTracks = tracks => {
  return {
    type: RESET_TRACKS,
    payload: {tracks},
  }
}

export const saveOptions = options => {
  return {
    type: SAVE_OPTIONS,
    payload: {options},
  }
}