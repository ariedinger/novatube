import {combineReducers} from 'redux';

import tracklist from './reducers/tracklist';
import options from './reducers/options';
import player from './reducers/player';
import sources from './reducers/sources';

export default combineReducers({
  tracklist,
  options,
  player,
  sources,
});
