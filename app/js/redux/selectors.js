import get from 'lodash.get';
import { createSelector } from 'reselect';
import { last } from '../utils/array';
import { isEqual } from '../utils/track';

export const selectSources = state => state.sources;
export const selectOptions = state => state.options;

export const selectIsPlaying = state => state.player.isPlaying;

export const selectTracks = state => state.tracklist;
export const selectCurrentTrack = createSelector(
  selectTracks,
  tracks => tracks.find(t => t.isCurrent)
);

export const selectIsLastTrack = createSelector(
  selectTracks,
  selectCurrentTrack,
  (tracks, currentTrack) => isEqual(currentTrack, last(tracks))
);
