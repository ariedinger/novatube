import {
  SET_SOURCES,
} from '../actions';

export const INITIAL_STATE = [];

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_SOURCES:
      return action.payload.sources;
    default:
      return state;
  };
};