import {
  RESUME_PLAYER,
  PLAY_TRACK,
  PAUSE_PLAYER,
} from '../actions';

export const INITIAL_STATE = {
  isReady: false,
  isPlaying: false,
  currentTime: 0,
  videoId: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PLAY_TRACK:
    case RESUME_PLAYER:
      return {
        ...state,
        isPlaying: true,
      }
    case PAUSE_PLAYER:
      return {
        ...state,
        isPlaying: false,
      }
    default:
      return state;
  };
};