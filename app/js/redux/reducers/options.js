import get from 'lodash.get';
import {
  SAVE_OPTIONS,
  PLAY_TRACK,
  SET_SOURCES,
} from '../actions';
export const INITIAL_STATE = {
  sources: null,
  chronological: true,
};

const deriveOptionsFromSources = (options, sources) => {
  return {
    ...options,
    sources: Object.fromEntries(sources.map(s => [s.key, get(options, `sources.${s.key}`) || s.defaultLevel]))
  }
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_SOURCES:
      return deriveOptionsFromSources(state, action.payload.sources);
    // case PLAY_TRACK:
    //   return {
    //     ...state,
    //     timestamp: action.payload.track.lastPlayedAtTimestamp,
    //   }
    case SAVE_OPTIONS:
      return {
        ...state,
        ...action.payload.options,
      };
    default:
      return state;
  };
};