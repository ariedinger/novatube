import {
  APPEND_TRACKS,
  RESET_TRACKS,
  PLAY_TRACK,
  NEXT_TRACK,
} from '../actions';
import {isEqual} from '../../utils/track';

const INITIAL_STATE = [];

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case RESET_TRACKS:
      return action.payload.tracks;
    case APPEND_TRACKS:
      return state.concat(action.payload.tracks);
    case PLAY_TRACK:
      return state.map(t => ({
        ...t,
        isCurrent: isEqual(t, action.payload.track),
      }));
    case NEXT_TRACK:
      const currentTrackIndex = state.findIndex(t => t.isCurrent);
      return state.map((t, i) => ({
        ...t,
        isCurrent: i === currentTrackIndex + 1,
      }));
    default:
      return state;
  };
};