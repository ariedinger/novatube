import React from 'react';

class Help extends React.Component {
  render(){
    return (
      <span className='icon-help-circled tooltip-holder'><div className='tooltip'>{this.props.tooltip}</div></span>
    );
  }
};

export default Help;