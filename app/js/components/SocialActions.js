import React from 'react';

class SocialActions extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      modalOpened: false,
    }
  }

  dislike() {
    this.props.track.dislike().then(() => {
      this.props.next();
    });
  }
  replace(newYoutubeID) {
    return this.props.track.replace(newYoutubeID).then(track => {
      this.props.play(track);
    });
  }
  share() {
    window.open(`https://www.facebook.com/sharer/sharer.php?u=${escape(window.location.href)}&t=${document.title}`, 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
  }
  showModal() {
    this.setState({
      modalOpened: true
    });
  }
  hideModal() {
    this.setState({
      modalOpened: false
    });
  }
  renderChildren() {
    // ReactDOM.render( <button onClick={e => this.props.track.like()} className="btn btn-like btn-block" title="Like Track" data-active={this.props.track.isLiked}><Icon name="heart"/> {this.props.track.isLiked ? 'Liked' : 'Like'}</button>, document.getElementById('like'));
    ReactDOM.render( <a href="#flag-modal" onClick={e => this.showModal()} title="Not the correct track?">Not the correct track?</a>, document.getElementById('flag'));
    ReactDOM.render( <button onClick={e => this.share()} className="btn btn-facebook btn-block" title="Share Track"><Icon name="facebook" /> Share</button>, document.getElementById('share'));

    if (this.props.track.isDisliked) {
      ReactDOM.render( <button onClick={e => this.props.track.dislike()} className="btn btn-like btn-lg" title="Unblock" data-active={true}>Unblock</button>, document.getElementById('dislike'));
    } else {
      ReactDOM.render( <button onClick={e => this.dislike()} className="btn btn-like btn-lg" title="Block and Next" data-active={false}><Icon name="block" /> + <Icon name="to-end-alt" /></button>, document.getElementById('dislike'));
    }
    ReactDOM.render( <NotCorrectTrackModal track={this.props.track} visible={this.state.modalOpened} clickAway={e => this.hideModal()} />, document.getElementById('flag-modal'));
  }
  componentDidMount(){
    this.renderChildren();
  }
  componentDidUpdate(){
    this.renderChildren();
  }
  render() {
    return (<div></div>);
  }
};

export default SocialActions;