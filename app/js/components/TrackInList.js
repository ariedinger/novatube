import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { playTrack } from '../redux/actions.js';
import { sourceString } from '../utils/track';
import { pluralize } from '../utils/utils';
import TimeAgo from './TimeAgo';
import Icon from './Icon';
import {
  selectSources,
} from '../redux/selectors';

const TrackInList = ({track, playTrack, sources}) => {
  const classes = ['track']
    .concat(track.isCurrent && 'active')
    .concat(track.isLiked && 'liked')
    .concat(track.isDisliked && 'disliked')
    .concat(track.isSkipped && 'skipped')
    .filter(Boolean).join(' ');

    const coverTitle = track.isLiked ? 'You like this track' : (track.isDisliked ? 'You blocked this track' : null);

  return (
    <li className={classes} onClick={e => playTrack(track)}>
      <small className='when'>
        <span> On {sourceString(track, sources)} </span>
        <TimeAgo date={track.lastPlayedAtTimestamp} />
        <ul className='list-inline text-right'>
          {track.likeCount > 0 && (
            <li title={`Liked ${pluralize(track.likeCount, 'time')} on NovaTube`}>
              <Icon name='thumbs-up' /> {track.likeCount}
            </li>
          )}
          {track.dislikeCount > 0 && (
            <li title={`Disliked ${pluralize(track.dislikeCount, 'time')} on NovaTube`}>
              <Icon name='thumbs-down' /> {track.dislikeCount}
            </li>
          )}
          {track.playCount > 0 && (
            <li title={`Played ${pluralize(track.playCount, 'time')} on NovaTube`}>
              <Icon name='play' /> {track.playCount}
            </li>
          )}
        </ul>
      </small>
      <div className='cover' title={coverTitle}>
        <img src={track.coverWithDefault} />
      </div>
      <span className='title'>{track.title}</span>
      <div>
        <span> by </span>
        <span className='artist'>{track.artist}</span>
      </div>
    </li>
  );
}

const mapStateToProps = createStructuredSelector({
  sources: selectSources,
});

const mapDispatchToProps = {
  playTrack,
};

export default connect(mapStateToProps, mapDispatchToProps)(TrackInList);