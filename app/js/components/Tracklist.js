import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  selectCurrentTrack,
  selectIsLastTrack,
  selectTracks,
  selectOptions,
  selectSources,
} from '../redux/selectors';
import { appendTracks, resetTracks, playTrack } from '../redux/actions.js';
import {fetchTracks} from '../utils/api';
import {key} from '../utils/track';
import TrackInList from './TrackInList';

const INIT_TRACK_ID = (window.location.pathname.match(/^\/track\/([\w-]{11})$/) || [])[1];

const nowTimestamp = () => (new Date()).getTime();

const Tracklist = () => {
  const dispatch = useDispatch();
  const currentTrack = useSelector(selectCurrentTrack);
  const isLastTrack = useSelector(selectIsLastTrack);
  const tracks = useSelector(selectTracks);
  const options = useSelector(selectOptions);
  const sources = useSelector(selectSources);

  const [lastFetchAt, setLastFetchAt] = useState(0);
  const canRefetch = nowTimestamp() - lastFetchAt > 1000 * 60 * 2; // 2 minutes

  const fetchAndStoreTracks = async (fetchOptions) => {
    if (fetchOptions.reset || canRefetch) {
      const tracks = await fetchTracks({...options, ...(fetchOptions || {})});
      if (fetchOptions.reset) {
        dispatch(resetTracks(tracks));
        dispatch(playTrack(tracks[0]))
      } else {
        dispatch(appendTracks(tracks));
      }
      setLastFetchAt(nowTimestamp());
    }
  }

  useEffect(() => {
    if (isLastTrack)
      fetchAndStoreTracks({timestamp: currentTrack.lastPlayedAtTimestamp});
  }, [currentTrack]);

  useEffect(() => {
    if (sources.length)
      fetchAndStoreTracks({reset: true, startWith: INIT_TRACK_ID})
  }, [options])

  return (
    <div id='trackscroll'>
      <div id='tracklist'>
        <ul>
          {tracks.map(track => <TrackInList key={key(track)} track={track} />)}
        </ul>
      </div>
    </div>
  );
}

export default Tracklist;