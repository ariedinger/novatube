import React, {useState, useEffect} from 'react';
import { useSelector } from 'react-redux';
import {
  selectIsPlaying,
} from '../redux/selectors';
import {
  secsToTime,
} from '../utils/utils';

export default ({ isReady, duration, getCurrentTime, setCurrentTime}) => {
  const isPlaying = useSelector(selectIsPlaying);

  const [mouseTime, setMouseTime] = useState(null);
  const [videoTime, setVideoTime] = useState(getCurrentTime());
  const [progressInterval, setProgressInterval] = useState(null);

  const time = mouseTime || videoTime || 0;
  const playedPercentage = parseInt(100 * time/duration)

  const eventToTime = e => {
    var {left, right} = e.target.getBoundingClientRect();
    var percentage = (e.clientX - left) / (right - left);
    return percentage * duration;
  }

  const onZoneClick = e => setCurrentTime(eventToTime(e));
  const onMouseMove = e => setMouseTime(eventToTime(e));
  const onMouseLeave = e => setMouseTime(null);

  const startInterval = () => {
    const intervalId = setInterval(() => {
      setVideoTime(getCurrentTime());
    }, 1000)
    setProgressInterval(intervalId);
  };

  const stopInterval = () => {
    if (progressInterval) {
      clearInterval(progressInterval);
      setProgressInterval(null);
    }
  };

  useEffect(() => {
    if (isReady && isPlaying) {
      startInterval();
    } else {
      stopInterval();
    }
    return stopInterval;
  }, [isPlaying, isReady]);

  return (
    <div id="progressBar">
      <div className='time'>{secsToTime(time)} / {secsToTime(duration)}</div>
      <div className='played' style={{width: `${playedPercentage}%`}} />
      <div className='clickzone' onClick={onZoneClick} onMouseMove={onMouseMove} onMouseLeave={onMouseLeave} />
    </div>
  )
}