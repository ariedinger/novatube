'use strict';
import React from 'react';

class TimeAgo extends React.Component {

  componentDidMount() {
    this.tick()
  }

  componentWillUnmount() {
    clearTimeout(this.state.timeout);
  }

  tick() {
    let then = this.props.date;
    let now = Date.now()
    let seconds = Math.round(Math.abs(now - then) / 1000)

    let period;

    if (seconds < 60) {
      period = 1000
    } else if (seconds < 60 * 60) {
      period = 1000 * 60
    } else if (seconds < 60 * 60 * 24) {
      period = 1000 * 60 * 60
    } else {
      period = 1000 * 60 * 60 * 24
    }

    this.setState({
      timeout: setTimeout(() => this.tick(), period)
    });
  }

  timeAgoString() {
    let then = this.props.date;
    let now = Date.now()
    let seconds = Math.round(Math.abs(now - then) / 1000)

    let value
    let unit

    if (seconds < 60) {
      value = Math.round(seconds)
      unit = 'second'
    } else if (seconds < 60 * 60) {
      value = Math.round(seconds / 60)
      unit = 'minute'
    } else if (seconds < 60 * 60 * 24) {
      value = Math.round(seconds / (60 * 60))
      unit = 'hour'
    } else if (seconds < 60 * 60 * 24 * 7) {
      value = Math.round(seconds / (60 * 60 * 24))
      unit = 'day'
    } else if (seconds < 60 * 60 * 24 * 30) {
      value = Math.round(seconds / (60 * 60 * 24 * 7))
      unit = 'week'
    } else if (seconds < 60 * 60 * 24 * 365) {
      value = Math.round(seconds / (60 * 60 * 24 * 30))
      unit = 'month'
    } else {
      value = Math.round(seconds / (60 * 60 * 24 * 365))
      unit = 'year'
    }
    return `${value} ${unit}${value !== 1 ? 's' : ''} ${then < now ? 'ago' : 'from now'}`;
  }

  render() {
    return React.createElement('time', {dateTime: new Date(this.props.date)}, this.timeAgoString())
  }
};

export default TimeAgo;