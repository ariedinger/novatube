import React from 'react';

class Icon extends React.Component {
  render(){
    return (
      <span className={`icon-${this.props.name} ${this.props.extraClass || ''}`}></span>
    );
  }
};

export default Icon;