import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  selectCurrentTrack,
  selectSources,
} from '../redux/selectors';

import TimeAgo from './TimeAgo';
import Inflector from './Inflector';
import {sourceString} from '../utils/track';
import {stripString} from '../utils/utils';

const PlayingTrack = ({currentTrack, sources}) => {
  const stats = [{
    text: 'Played',
    display: true,
    noComma: true,
    jsx: <Inflector word='time' nb={currentTrack.playCount} />,
  }, {
    text: 'on',
    display: true,
    jsx: sourceString(currentTrack, sources),
  }, {
    text: 'last time',
    display: true,
    jsx: <TimeAgo date={currentTrack.lastPlayedAtTimestamp} />,
  }, {
    text: 'liked',
    display: Boolean(currentTrack.likeCount),
    jsx: <Inflector word="time" nb={currentTrack.likeCount} />,
  }, {
    text: 'disliked',
    display: Boolean(currentTrack.dislikeCount),
    jsx: <Inflector word="time" nb={currentTrack.dislikeCount} />,
  }].filter(content => content.display)
    .map((el, i, array) => (
      <span key={el.text}>
        {el.text}&nbsp;
        <strong>{el.jsx}</strong>
        {!el.noComma && i !== (array.length - 1) ? ',' : ''}&nbsp;
        </span>
    ));
  return (
    <div>
      <div className='infos'>
        <span className='no-line-break' title={currentTrack.title}><strong>{stripString(currentTrack.title)}</strong></span>
        <span className='no-line-break' title={currentTrack.artist}><small> by </small><strong>{currentTrack.artist}</strong></span>
      </div>
      <div className='stats'>{stats}</div>
    </div>
  )
}

const mapStateToProps = createStructuredSelector({
  currentTrack: selectCurrentTrack,
  sources: selectSources,
});

export default connect(mapStateToProps)(PlayingTrack);