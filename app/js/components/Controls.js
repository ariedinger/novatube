import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import get from 'lodash.get';
import {
  selectCurrentTrack,
  selectIsPlaying,
} from '../redux/selectors';
import {
  nextTrack,
  resumePlayer,
  pausePlayer,
} from '../redux/actions.js';

import Icon from './Icon';

const Controls = () => {
  const dispatch = useDispatch();
  const currentTrack = useSelector(selectCurrentTrack);
  const isPlaying = useSelector(selectIsPlaying);

  const togglePlay = e => {
    if (isPlaying) {
      return dispatch(pausePlayer());
    }
    else {
      return dispatch(resumePlayer());
    }
  };

  const like = e => {
    // TODO write this;
  }

  const dislike = e => {
    // TODO write this;
  }

  return (
    <div id="controls">
      <div className='controls-wrapper'>
        <div className='actions'>
          <button onClick={togglePlay} className='btn btn-control' title={isPlaying ? 'Pause' : 'Play'} >
            <Icon name={isPlaying ? 'pause' : 'play'}/>
          </button>
          <button onClick={e => dispatch(nextTrack())} className='btn btn-control' title='Next Track' >
            <Icon name='to-end-alt'/>
          </button>
        </div>
        <div className='socials'>
          <button
            onClick={like}
            className={`btn btn-control ${get(currentTrack, 'isLiked') ? 'active' : ''}`}
            title={get(currentTrack, 'isLiked') ? 'Undo like track' : 'Like Track'}
          >
            <Icon name='thumbs-up' />
          </button>
          <button
            onClick={dislike}
            className={`btn btn-control ${get(currentTrack, 'isDisliked') ? 'active' : ''}`}
            title={get(currentTrack, 'isDisliked') ? 'Undo dislike track' : 'Dislike Track'}
          >
            <Icon name='thumbs-down' />
          </button>
        </div>
      </div>
    </div>
  );
};

export default Controls;