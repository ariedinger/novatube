import React from 'react';
import {pluralize} from '../utils/utils';

const Inflector = ({nb, word}) => {
  return (<span>{pluralize(nb, word)}</span>);
}

export default Inflector;