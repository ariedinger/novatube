import React from 'react';

const Header = props =>{
  return (
    <div id="title">
      <h1><a href="/">NovaTube</a></h1>
      <div className="separation"></div>
      <p id="subtitle">
        <span>Continuous <a href="https://www.nova.fr" target="_blank">Nova</a> playlist based on </span>
        <a href="https://www.nova.fr/c-etait-quoi-ce-titre" target="_blank">last played tracks</a>.
        <br /><br />
        <a href="/about">&gt; Learn about the project &lt;</a>
      </p>
    </div>
  )
}

export default Header;