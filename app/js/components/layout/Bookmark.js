import React from 'react';

const Bookmark = props =>{
  return (
    <div className="hidden-xs hidden-sm" id="bookmarkme">
      <span className="icon-star"></span>
      <a className="link" href="/" title="Bookmark Novatube to save your profile (liked &amp; skipped tracks)">NovaTube</a>
    </div>
  )
}

export default Bookmark;