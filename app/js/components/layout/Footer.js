import React from 'react';

const Footer = props =>{
  return (
    <footer className="text-center">
      <p>
        <span>By using Novatube, the user accepts </span>
        <a href="https://www.youtube.com/t/terms" target="_blank">YouTube Terms Of Services</a>
        <span> and </span>
        <a href="https://policies.google.com/privacy" target="_blank">Google Privacy policy</a>.
      </p>
      <hr />
      <p>Made for the<span className="icon-heart" title="Love"></span> of music:</p>
    </footer>
  )
}

export default Footer;