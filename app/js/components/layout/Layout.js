import React from 'react';
import Header from './Header';
import Footer from './Footer';
import Bookmark from './Bookmark';

const Layout = ({children}) => {
  return (
    <div className="container-fluid">
      <Header />
      {children}
      <Footer />
      <Bookmark />
    </div>
  );
};

export default Layout;
