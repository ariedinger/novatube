import React from 'react';

class NotCorrectTrackModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      thanks: null,
      newYoutubeID: null
    };
  }
  onClick(evt) {
    if (this.state.newYoutubeID) {
      if (this.state.newYoutubeID !== this.props.track.youtubeID) {
        this.setState({
          thanks: 'Submitting...'
        })
        this.props.track.replace(this.state.newYoutubeID).then(() => {
          this.setState({
            thanks: 'Thanks for Submition!'
          });
          setTimeout(function(){
            $('#flag-modal').modal('hide');
          }, 2000);
        });
      } else {
        this.setState({
          thanks: 'This is the same track'
        });
      }
    } else {
      this.setState({
        thanks: 'No URL provided'
      });
    }
  }
  handleChange(evt) {
    var youtubeID = Utils.queryString(evt.target.value).v
    if (youtubeID) {
      if (youtubeID === this.props.track.youtubeID) {
        this.setState({
          thanks: 'This is the same track'
        });
      } else {
        this.setState({
          newYoutubeID: youtubeID
        });
      }
    } else {
      this.setState({
        thanks: 'Incorrect URL format'
      });
    }
  }
  render(){
    console.log('Rendering!')
    return (
      <Modal visible={this.props.visible} width="400" height="300" onClickAway={() => this.props.clickAway()}>
        <div>
          <h1>Title</h1>
          <p>Some Contents</p>
          <button onClick={() => this.closeModal()}>Close</button>
        </div>
      </Modal>
    );
  }
};

export default NotCorrectTrackModal;