import React, {useEffect, useRef} from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {selectCurrentTrack} from '../redux/selectors';

import {
  selectSources,
} from '../redux/selectors';


import PlayingTrack from './PlayingTrack';
import Options from './Options';
import Controls from './Controls';
import ProgressBar from './ProgressBar';
import Tracklist from './Tracklist';
import Player from './Player';
import {sourceString} from '../utils/track';
import {scrollTo} from '../utils/utils';

const App = ({currentTrack, sources}) => {
  const $optionsButton = useRef(null);
  const scrollToCurrentTrack = () => {
    const $tracklist = document.getElementById('tracklist');
    const activeTrack = $tracklist.querySelector('li.track.active');
    const tracks = Array.from($tracklist.querySelectorAll('li.track'));
    const beforeActiveTracks = tracks.splice(0, tracks.indexOf(activeTrack));
    const scroll = beforeActiveTracks.map(track => track.clientHeight).reduce((res, value) => res + value, 0);
    scrollTo($tracklist, scroll, 500);
  }

  useEffect(() => {
    if (!currentTrack) return;
    document.title = `${currentTrack.title} by ${currentTrack.artist} on ${sourceString(currentTrack, sources)} - NovaTube`;
    history.pushState(currentTrack.youtubeID, document.title, `/track/${currentTrack.youtubeID}`);
    ga('send', 'event', 'player', 'play', currentTrack);
    scrollToCurrentTrack();
  }, [currentTrack])

  return (
    <div className="row">
      <div className="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
        <div className="box">
          <div className="row">
            <div className="col-sm-12">
              <div className="now-playing">
                <div className="row">
                  <div className="col-sm-8 col-sm-push-2" id="now">
                    {currentTrack ? <PlayingTrack /> : <h4>Chargement en cours ...</h4>}
                  </div>
                  <div className="col-sm-2 col-sm-pull-8 marged-small-screen col-xs-6">
                    <button className="btn btn-facebook btn-block" title="Share Track on Facebook">
                      <span className="icon-facebook" />
                      <span className="hidden-md hidden-sm">Share</span>
                    </button>
                  </div>
                  <div className="col-sm-2 marged-small-screen col-xs-6">
                    <Options buttonRef={$optionsButton} />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12">
              <div className="player-wrapper">
                <Player />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12 controls">
              <Controls />
            </div>
            <div className="col-sm-12">
              <Tracklist />
            </div>
            <div className="col-sm-12">
              <p className="more-sources-tip">
                <strong>TIP : </strong>
                Need more music ? <a onClick={e => $optionsButton.current.click()}>Check the options</a>, other radio sources are available (FIP, Radio Sympa ...)
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}


const mapStateToProps = createStructuredSelector({
  currentTrack: selectCurrentTrack,
  sources: selectSources,
});

export default connect(mapStateToProps)(App);