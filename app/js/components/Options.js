import get from 'lodash.get';
import React, {useState} from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  selectOptions,
  selectSources,
  selectCurrentTrack,
} from '../redux/selectors';
import {
  saveOptions,
  setSources,
} from '../redux/actions';
import {AUTO_SKIP_OPTION_STEP} from '../utils/constants';
import {capitalize} from '../utils/utils';
import {fetchSources} from '../utils/api';

import Modal from './Modal';
import Help from './Help';
import Icon from './Icon';

const DateElInput = ({type, date, onChange}) => {
  const monthFactor = type === 'Month' ? 1 : 0;
  const value = ((new Date(date))[`get${type}`]() + monthFactor).toString().padStart(2, '0');
  const style = {width: `${type === 'FullYear' ? 3 : 2}em`};
  const onInputChange = e => onChange(new Date(date)[`set${type}`](e.target.value - monthFactor));

  return <input style={style} onChange={onInputChange} type='number' value={value} />
}

const Options = ({options, sources, saveOptions, setSources, buttonRef, currentTrack}) => {
  const [isModalOpened, setModalOpened] = useState(false);
  const [stateOptions, setStateOptions] = useState(options);

  const getAvailableSourcesAndSaveOptions = async () => {
    setSources(await fetchSources());
  }

  if (sources.length === 0) getAvailableSourcesAndSaveOptions()

  const openModal = () => setModalOpened(true);
  const closeModal = () => setModalOpened(false);

  const updateTimestamp = timestamp => setStateOptions({
    ...stateOptions,
    timestamp,
  });
  const updateSourceLevels = (field, value) => setStateOptions({
    ...stateOptions,
    sources: {
      ...stateOptions.sources,
      [field]: value,
    }
  });
  const updateChronological = chronological => setStateOptions({
    ...stateOptions,
    chronological,
  });

  const sourceDisplay = source => {
    const value = get(stateOptions, `sources.${source.key}`);
    return (
      <li key={source.key}>
        {source.name}
        <input type='range' min='0' max={5 * AUTO_SKIP_OPTION_STEP} step={AUTO_SKIP_OPTION_STEP} onChange={e => updateSourceLevels(source.key, parseInt(e.target.value))} value={value} />
        <p>
          <em>
            {value === 0 ? (
              <span>Source disabled</span>
            ) : (
              <span>Source activated:<br /> autoskip after {value} plays</span>
            )}</em></p>
      </li>
    )
  }

  const save = () => {
    window.location.pathname = '/'; // Make sure startWith option is not set;
    saveOptions(stateOptions);
    closeModal();
  }

  const timeChanger = timestamp => {
    if (!timestamp) return;
    return (
      <div className='date-changer'>
        <DateElInput date={timestamp} type='Date' onChange={updateTimestamp} />
        /
        <DateElInput date={timestamp} type='Month' onChange={updateTimestamp} />
        /
        <DateElInput date={timestamp} type='FullYear' onChange={updateTimestamp} />

        <DateElInput date={timestamp} type='Hours' onChange={updateTimestamp} />
        :
        <DateElInput date={timestamp} type='Minutes' onChange={updateTimestamp} />
      </div>
    );
  };

  return (
    <div className='options'>
      <button className='btn btn-like btn-block' ref={buttonRef} onClick={e => openModal()}>
        <Icon name='cog' />
        <span className='hidden-md hidden-sm'>Options</span>
      </button>
      <Modal visible={isModalOpened} width="400" onClickAway={e => closeModal()}>
        <div>
          <button onClick={e => closeModal()} className='close'>
            <Icon name='cancel'/>
          </button>
          <h5>Options</h5>
          <h6 className='helper'>Playing Time &amp; Direction <Help tooltip='At what time and in which direction do you want the NovaTube to play?' /></h6>
          {timeChanger(stateOptions.timestamp || get(currentTrack, 'lastPlayedAtTimestamp'))}
          <button onClick={e => updateChronological(false)} className={`btn btn-like btn-toggle ${stateOptions.chronological ? '' : 'active'}`}>
            <Icon name='back-in-time' /> Back-in-time
          </button>
          <button onClick={e => updateChronological(true)} className={`btn btn-like btn-toggle ${stateOptions.chronological ? 'active' : ''}`}>
            <Icon name='back-in-time' extraClass='mirror' /> Chronological
          </button>
          <hr className='visible' />
          <h6 className='helper'>Radio Sources <Help tooltip='Which radio do you want to turn on?' /></h6>
          <ul className='autoskip'>
            {sources.map(sourceDisplay)}
          </ul>
          <hr />
          <button className='btn btn-like btn-lg' onClick={e => closeModal()}><Icon name='cancel'/> Cancel</button>
          &nbsp;
          &nbsp;
          <button className='btn btn-like btn-lg' onClick={e => save()}><Icon name='floppy'/> Apply</button>
        </div>
      </Modal>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  options: selectOptions,
  sources: selectSources,
  currentTrack: selectCurrentTrack,
});

const mapDispatchToProps = {
  saveOptions,
  setSources,
};

export default connect(mapStateToProps, mapDispatchToProps)(Options);