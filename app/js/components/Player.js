import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  selectCurrentTrack,
  selectIsPlaying,
} from '../redux/selectors';
import {
  nextTrack,
  resumePlayer,
  pausePlayer,
} from '../redux/actions.js';
import YouTube from 'react-youtube';
import ProgressBar from './ProgressBar';

const PLAYER_OPTIONS = {
  width: '100%',
  height: '100%',
  playerVars: {
    // https://developers.google.com/youtube/player_parameters
    autoplay: 1,
    controls: 0,
    showinfo: 0,
    modestbranding: 1,
    autohide: 1,
    iv_load_policy: 3,
    rel: 0,
    fs: 0,
  },
}
export const Player = () => {
  const dispatch = useDispatch();
  const currentTrack = useSelector(selectCurrentTrack);
  const isPlaying = useSelector(selectIsPlaying);

  const [player, setPlayer] = useState(null);
  const [duration, setDuration] = useState(0);

  const onPlayerReady = e => {
    setPlayer(e.target);
    dispatch(resumePlayer());
  }

  const onStateChange = e => {
    switch (e.data) {
      case YT.PlayerState.ENDED:
        return dispatch(nextTrack());
      case YT.PlayerState.UNSTARTED:
      case YT.PlayerState.CUED:
      case YT.PlayerState.PLAYING:
        setDuration(e.target.getDuration())
        return !isPlaying && dispatch(resumePlayer());
      case YT.PlayerState.PAUSED:
        return dispatch(pausePlayer());
      case YT.PlayerState.BUFFERING:
        return; // Nothing
      default:
        return console.warn('UNKNOWN_STATE', e);
    }
  }

  useEffect(() => {
    if (!player) return;
    isPlaying ? player.playVideo() : player.pauseVideo();
  }, [isPlaying])

  const safeGetCurrentTime = () => {
    return player && player.getCurrentTime();
  }

  const safeSetCurrentTime = time => {
    return player && player.seekTo(time);
  }

  return (
    <div id="player">
      {(!player || !currentTrack) && <span className="icon-youtube" />}

      <YouTube
        videoId={currentTrack && currentTrack.youtubeID}
        opts={PLAYER_OPTIONS}
        onReady={onPlayerReady}
        onStateChange={onStateChange}
        onError={e => dispatch(nextTrack())}
      />
      <ProgressBar
        isReady={!!player}
        duration={duration}
        getCurrentTime={safeGetCurrentTime}
        setCurrentTime={safeSetCurrentTime}
      />
    </div>
  )
}

export default Player;