import React, {useEffect} from 'react';

const STYLES = {
  'mask': {
    'position': 'fixed',
    'top': 0,
    'left': 0,
    'width': '100%',
    'height': '100%',
    'backgroundColor': 'rgba(0, 0, 0, 0.7)',
    'zIndex': 10001
  },
  'maskHidden': {
    'display': 'none'
  },
  'container': {
    'position': 'fixed',
    'top': 0,
    'left': 0,
    'width': '100%',
    'height': '100%',
    'display': 'flex',
    'justifyContent': 'center',
    'alignItems': 'center',
    'zIndex': 10000
  },
  'containerHidden': {
    'position': 'fixed',
    'top': 0,
    'left': 0,
    'width': '100%',
    'height': '100%',
    'display': 'flex',
    'justifyContent': 'center',
    'alignItems': 'center',
    'zIndex': -1
  },
  'panel': {
    'boxSizing': 'border-box',
    'boxShadow': '0 2px 4px rgba(0, 0, 0, 0.3)',
    'transform': 'translate3d(0, 0, 0)',
    'transition': 'transform 500ms cubic-bezier(0, 0, 0.25, 1), opacity 500ms cubic-bezier(0, 0, 0.25, 1)',
    'zIndex': 10002
  },
  'panelHidden': {
    'transform': 'translate3d(0, -100px, 0)',
    'opacity': 0,
    'zIndex': -1,
    'height': 0,
    'width': 0,
    'overflow': 'hidden'
  }
};

const Modal = ({visible, width, height, children, onClickAway}) => {

  const panelStyleWithDimensions = {
    ...STYLES.panel,
    ...(width ? {width: `${width}px`} : {}),
    ...(height ? {height: `${height}px`} : {}),
  };

  useEffect(() => {
    const listener = e => {
      if (e.key === 'Escape') onClickAway();
    }
    if (visible) {
      window.addEventListener('keydown', listener);
      return () => {
        window.removeEventListener('keydown', listener);
      }
    }
  }, [visible])

  return (
    <div>
      <div style={visible ? STYLES.container : STYLES.containerHidden}>
        <div style={visible ? panelStyleWithDimensions : STYLES.panelHidden} className='modal'>
          {children}
        </div>
        <div style={visible ? STYLES.mask : STYLES.maskHidden} onClick={onClickAway ? onClickAway : false} />
      </div>
    </div>
  );
}

export default Modal;