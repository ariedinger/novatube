import './utils/analytics';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import setupStore from './redux/setupStore';

import RouterPage from './pages/RouterPage';

ReactDOM.render(
  <Provider store={setupStore()}>
    <RouterPage />
  </Provider>,
  document.getElementById('app')
);