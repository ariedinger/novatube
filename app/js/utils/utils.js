'use strict';

export const facebookShare = () => {
  window.open(`https://www.facebook.com/sharer/sharer.php?u=${escape(window.location.href)}&t=${document.title}`, 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
}

export const localStorage = (key, value) => {
  if(typeof(Storage) !== 'undefined' && typeof(localStorage) === 'object') {
    if (value) {
      return localStorage.setItem(key, JSON.stringify(value));
    } else {
      const content = localStorage.getItem(key);
      return content && JSON.parse(content);
    }
  }
}

export const twoZeros = number => { // TODO change to a pad function
  if (number < 10) {
    return '0'+number.toString();
  } else {
    return number.toString();
  }
}

export const stripString = (string, length=40) => {
  if (string.length > length) {
    return `${string.substring(0, length)} (...)`
  }
  return string;
}

export const secsToTime = secs => {
  if (!secs) return '--:--';
  var secs = parseInt(secs);
  var mins = parseInt(secs / 60);
  var secsLeft = secs % 60;
  var hours = parseInt(mins / 60);
  var minsLeft = mins % 60;
  if (hours > 0) {
    return `${hours}:${twoZeros(minsLeft)}:${twoZeros(secsLeft)}`;
  } else {
    return `${mins}:${twoZeros(secsLeft)}`;
  }
}

export const pluralize = (count, word) => {
  if (word === 'time' && count === 1) {
    return 'once';
  }
  return `${count} ${word}${count>1?'s':''}`;
}

export const capitalize = string => string.charAt(0).toUpperCase() + string.slice(1)

export const easeInOut = (currentTime, start, change, duration) => {
  currentTime /= duration / 2;
  if (currentTime < 1) {
      return change / 2 * currentTime * currentTime + start;
  }
  currentTime -= 1;
  return -change / 2 * (currentTime * (currentTime - 2) - 1) + start;
}

export const scrollTo = (element, to, duration) => {
  duration = duration || 1000;
  let start = element.scrollTop,
      change = to - start,
      increment = 20;

  let animateScroll = (elapsedTime) => {
      elapsedTime += increment;
      let position = easeInOut(elapsedTime, start, change, duration);
      element.scrollTop = position;
      if (elapsedTime < duration) {
        setTimeout(() => animateScroll(elapsedTime), increment);
      }
  };

  animateScroll(0);
}
