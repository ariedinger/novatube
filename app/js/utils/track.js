export const key = track => `${track.lastPlayedAtTimestamp}@${track.source}`;
export const isEqual = (t1, t2) => t1 && t2 && key(t1) === key(t2);

export const sourceString = (track, sources) => {
  const source = sources.find(s => s.key === track.source);
  return source && source.name || track.source;
};