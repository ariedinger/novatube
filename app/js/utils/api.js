
const queryString = obj => {
  return Object.entries(obj)
    .filter(([k, v]) => Boolean(v))
    .map(([k, v]) => `${k}=${v}`)
    .join('&');
}

const apiOptions = options => {
  return {
    reversed: options.reversed,
    sources: Object.entries(options.sources || {})
      .filter(([k, v]) => v > 0)
      .map(([k, v]) => `${k}_${v}`)
      .join('-'),
    timestamp: options.timestamp,
    startWith: options.startWith,
  }
};

export const fetchApi = async (url, fetchOptions) => {
  const result = await fetch(url, {credentials: 'same-origin', ...fetchOptions});
  return await result.json();
}

export const fetchTracks = async options => (await fetchApi(`/api/tracks?${queryString(apiOptions(options))}`));
export const fetchSources = async () => (await fetchApi('/api/sources'));