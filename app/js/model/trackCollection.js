const CSRF_TOKEN = ''; // document.querySelector('meta[name="csrf-token"]').attributes.content.value;

const fetchApi = (url, options) => {
  return fetch(url, Object.assign({credentials: 'same-origin'}, options))
    .then(response => Promise.resolve(response.json()));
};

const postApi = (url) => fetchApi(url, {
  method: 'POST',
  headers: {
    'x-csrf-token': CSRF_TOKEN
  }
});

const INIT_TRACK_ID = (window.location.pathname.match(/^\/track\/([\w-]{11})$/) || [])[1];

class Track {
  constructor(jsonTrack) {
    return this.feedFromJson(jsonTrack);
  };

  get key() {
    return `${this.lastPlayedAtTimestamp}@${this.source}`;
  }

  get isSkipped() {
    return this.isDisliked || this.skipAt > -1 && (!this.isLiked && this.playCount > this.skipAt);
  }

  get confidenceString() {
    return `${this.confidence && (this.confidence * 100).toFixed() || 0}%`;
  }

  feedFromJson(jsonTrack){
    Object.keys(jsonTrack).forEach(key => {
      this[key] = jsonTrack[key];
    });
    return this;
  };

  smaller(otherTrack) {
    return otherTrack && (this.key < otherTrack.key)
  }

  equal(otherTrack){
    return otherTrack && (this.key === otherTrack.key);
  };

  like(){
    return postApi(`/api/track/${this.youtubeID}/like`).then(jsonTrack => this.feedFromJson(jsonTrack));
  };

  dislike(){
    return postApi(`/api/track/${this.youtubeID}/dislike`).then(jsonTrack => this.feedFromJson(jsonTrack));
  };

  replace(newYoutubeID){
    return postApi(`/api/track/${this.youtubeID}/replace/${newYoutubeID}`).then(jsonTrack => this.feedFromJson(jsonTrack));
  };
};

class TrackCollection {
  constructor(options) {
    this.tracks = [];
    // TODO: concurrency issue: find a better solution
    this.ready = fetchApi('/api/sources').then(sources => {
      this.sources = sources;
      let levels = {};
      sources.forEach(source => levels[source.key] = source.defaultLevel); // TODO: make it functional
      this.options = Object.assign({
        reversed: false,
        levels: levels,
      }, Utils.localStorage('NovaTubeOptions'), options);
    });
    return this;
  };

  upsertSorted(jsonTracks) {
    jsonTracks.forEach(jsonTrack => {
      const sourceLevel = parseInt(this.options.levels[jsonTrack.source]);
      let newTrack = new Track(Object.assign(jsonTrack, {
        skipAt: sourceLevel >= 5 ? -1 : AUTO_SKIP_OPTION_STEP * sourceLevel,
        sourceString: () => this.sources.find(s =>  s.key === (jsonTrack.source || 'nova')).name,
      }));
      let closestTrackIndex = this.tracks.findIndex(track => ((this.options.reversed !== newTrack.smaller(track)) || newTrack.equal(track)));
      if (newTrack.equal(this.tracks[closestTrackIndex])) {
        this.tracks[closestTrackIndex] = newTrack; // Update track
      } else if (closestTrackIndex === -1) {
        this.tracks.push(newTrack);
      } else {
        this.tracks.splice(closestTrackIndex, 0, newTrack); // Insert track
      }
    });
    return this;
  };

  length() {
    return this.tracks.length;
  }

  last() {
    return this.tracks[this.tracks.length - 1];
  }

  indexOf(track) {
    return this.tracks.findIndex(_track => _track.equal(track));
  };

  include(track) {
    return Boolean(this.tracks.find(_track => _track.equal(track)));
  };

  next(currentTrack) {
    return this.tracks.find((track, index) => {
      return (index > this.indexOf(currentTrack)) && !track.isSkipped;
    });
  };

  prev(currentTrack) {
    return this.tracks.reverse().find((track, index) => {
      return (index > this.indexOf(currentTrack)) && !track.isSkipped;
    });
  };

  apiOptions(timestamp) {
    return Object.assign({
      reversed: this.options.reversed,
      sources: Object.keys(this.options.levels).filter(key => this.options.levels[key] > 0).join('-')
    }, timestamp ? {timestamp: timestamp} : {startWith: INIT_TRACK_ID})
  };

  fetch(timestamp) {
    return this.ready
      .then(() => fetchApi(`/api/tracks?${Utils.queryString(this.apiOptions(timestamp))}`))
      .then(jsonTracks => this.upsertSorted(jsonTracks));
  };

};

export default TrackCollection;