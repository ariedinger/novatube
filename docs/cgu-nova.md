### **Editeur :**

**Le site novaplanet.com (ci-après « le Site ») est édité par  la société RADIO NOVA,** SARL au capital de 160.000 €, dont le siège social est situé au 127, avenue Ledru-Rollin 75011 Paris, immatriculée auprès du RCS de Paris sous le numéro 342 987 211

SIRET 342 987 211 00014

TVA Intracommunautaire FR 01 342 987 211

Tél : 01.53.33.33.00 [contact@novaplanet.com](mailto:contact@novaplanet.com)

### **Directeur de la publication :**

Monsieur Bruno DELPORT

### **Hébergeur :**

**Open Web Network Solutions**

Société à responsabilité limitée au capital de 228.000 €

Siège social : 23, rue du dessous des berges 75013 Paris

RCS Paris 528 146 053

Téléphone : 01 42 46 26 82

### **Loi informatique et libertés :**

**Données à caractère personnel**

Conformément à la loi « Informatique et Libertés » du 6 janvier 1978, modifiée par la loi du 6 août 2004, vous êtes informé(e) que la société Radio Nova  procède à des traitements automatisés de vos données personnelles recueillies dans le cadre des formulaires en ligne remplis par vos soins. 

(Déclaration CNIL n°1815721)

Les données personnelles collectées dans le cadre des inscriptions au Site sont susceptibles d'être utilisées par Radio Nova  exclusivement aux fins suivantes :

- envoi de newsletters, de propositions commerciales et/ou promotionnelles, émanant de Radio Nova  et concernant exclusivement les produits et services de Radio Nova.

- envoi de propositions commerciales et/ou promotionnelles, émanant de sociétés tierces en relation avec Radio Nova  et ne concernant pas exclusivement les produits et services de Radio Nova. Cet envoi sera effectué exclusivement par Radio Nova. Aucune vente de base de données à des sociétés tierces n'est faite par Radio Nova. Cet envoi de propositions commerciales et/ou promotionnelles sera effectué conformément à l'article L34-5 du Code des Postes et Télécommunications.

Conformément aux dispositions de la loi du 6 janvier 1978 relative aux fichiers, à l'informatique et aux libertés, vous disposez d'un droit d'accès, de modification,  de rectification et de suppression des données qui vous concernent. Pour exercer ce droit, adressez-vous à : RADIO NOVA - Service Web - 127, avenue Ledru-Rollin  75011 Paris.

(Déclaration CNIL n°1801974v0)

Les données personnelles collectées par Radio Nova dans le cadre des inscriptions aux jeux-concours organisés par Radio Nova sont exclusivement destinées à la gestion desdits jeux-concours. L’inscription à ces jeux-concours implique que vous acceptez que vos coordonnées soient transmises aux sociétés partenaires de RADIO NOVA et ce pour la gestion des dotations exclusivement.

Conformément aux dispositions de la loi du 6 janvier 1978 relative aux fichiers, à l'informatique et aux libertés, vous disposez d'un droit d'accès, de modification,  de rectification et de suppression des données qui vous concernent. Vous pouvez exercer ce droit par demande écrite adressée à : RADIO NOVA / « JEUX NOVA GRATOS»  127, avenue Ledru Rollin 75011 Paris. 

### Utilisation des Cookies

Un ou plusieurs "cookies" seront placés sur le disque dur de l'ordinateur des internautes afin qu'ils soient directement identifiés lorsqu'ils se connectent au site, de faciliter la gestion des comptes. Les internautes sont informés de la possibilité de s'opposer à l'installation de "cookies" sur son ordinateur. En aucun cas une corrélation n'est faite entre ces cookies et des informations nominatives que pourrait détenir Novaplanet.

**1. ****Qu'est-ce qu'un cookie ?**

Les cookies sont des petits programmes informatiques que le Site Internet place sur le disque dur de votre ordinateur, de votre tablette, ou de votre téléphone, qui ont pour but de collecter des informations relatives à votre navigation (pages consultées,  date et heure de consultation, etc.) et d’adresser des contenus et services adaptés à vos centres d’intérêts. Les cookies ne permettent pas de vous identifier en tant qu'utilisateur physique mais identifient votre ordinateur.

Il existe deux types de cookies : les cookies de sessions et les cookies persistants. Les cookies de session sont des bits temporaires d'information qui sont effacés quand vous quittez votre navigateur. Les cookies persistants sont des bits d'informations plus permanents qui sont stockés et restent sur votre ordinateur jusqu'à leur suppression. Les cookies persistants se suppriment automatiquement après un certain temps mais sont recréés à chaque fois que vous visitez le Site Internet.

Le Site Internet Novaplanet.com (ci-après « le Site Internet ») utilise des cookies de session et des cookies persistants. 

**2. ****Quels cookies sur le Site Internet ?**

**_a) Les cookies déposés par Nova :_**

- les cookies utilisés dans le cadre des mesures d’audience : ces cookies nous permettent de réaliser des statistiques de fréquentation de notre site et de ses différentes rubriques.

- les cookies traceurs de réseaux sociaux générés par les « boutons de partage réseaux sociaux » : les boutons de partage via les réseaux sociaux font appel aux cookies afin de suivre la navigation de leurs internautes.

- les cookies d’identification : les cookies d’identification sont utilisés dans le cadre de la création et/ou de la connexion à votre espace personnel sur l’un de nos sites ainsi que votre contribution dans les espaces participatifs. Ils vous permettent d’accéder à votre compte et/ou espace réserver grâce à vos identifiants et optimisent votre expérience, notamment en vous aidant à vous rappeler vos identifiant et mot de passe quand vous revenez sur le site et ainsi vous connectez sans avoir à vous identifier de nouveau.

**_b) Les cookies déposés par les tiers_**

Les partenaires et annonceurs du Site Internet utilisent des cookies pour identifier vos centres d’intérêts et personnaliser l’offre publicitaire qui vous est adressée sur et en dehors du Site Internet. Ces cookies sont susceptibles, sous réserve de vos choix, d'être enregistrés dans votre terminal et de permettre de reconnaître le navigateur de votre terminal pendant la durée de validité du cookie concerné et ainsi de suivre la navigation ultérieure effectuée par votre terminal sur des sites/applications ou sur d'autres contenus.

L’objectif est de vous présenter les publicités les plus pertinentes possibles. A cette fin, la technologie des cookies permet de déterminer en temps réel quelle publicité afficher sur un terminal, en fonction de sa navigation récente sur un ou plusieurs sites ou applications.

L'émission et l'utilisation de cookies par des tiers, sont soumises aux politiques de protection de la vie privée de ces tiers.  Nous avons ni accès et ni contrôle sur les cookies tiers. Toutefois nous demandons à ce que les sociétés partenaires traitent les informations collectées sur le Site Internet dans le respect de la loi «Informatique et Libertés ».

**3. ****Consentement préalable au dépôt des cookies**

Il est considéré que vous avez donné votre accord au dépôt des cookies si vous avez poursuivi votre navigation sur le Site Internet en cliquant sur un élément du site tel qu’une image, un titre, un bouton etc. ou en vous rendant sur toute autre page que la page par laquelle vous avez accéder à l’un de nos sites lors de votre première connexion.

Votre accord n’est valable que pour une durée de treize mois à compter du premier dépôt dans votre équipement terminal. Une fois ce délai dépassé, votre accord sera redemandé pour l’utilisation de ces cookies.

**4. ****La gestion / suppression  des cookies**

Plusieurs possibilités vous sont offertes pour gérer les cookies. Tout paramétrage que vous pouvez entreprendre sera susceptible de modifier votre navigation sur Internet et vos conditions d'accès à certains services nécessitant l'utilisation de Cookies.

**a) Via le paramétrage de votre navigateur internet**

Vous pouvez, à tout moment modifier vos souhaits concernant le dépôt de cookies via le paramétrage de votre navigateur internet.

Vous pouvez configurer votre logiciel de navigation de manière à ce que des cookies soient enregistrés dans votre terminal ou, au contraire, qu'ils soient rejetés, soit systématiquement, soit selon leur émetteur. Vous pouvez également configurer votre logiciel de navigation de manière à ce que l'acceptation ou le refus des cookies vous soient proposés ponctuellement, avant qu'un cookie soit susceptible d'être enregistré dans votre terminal.

Veuillez noter que notre Site Internet ne pourra pas fonctionner à son maximum si vous supprimez les cookies. Le refus des cookies entrainera notamment l’impossibilité de bénéficier de l’ensemble de nos services. 

**Comment exercer vos choix selon le navigateur que vous utilisez ?**

Si ces raccourcis ne fonctionnent pas sur votre navigateur, vous devez exercer vos choix selon le navigateur que vous utilisez.  En effet, la configuration de chaque navigateur est différente. Elle est décrite dans le menu d'aide du navigateur qui  vous permettra de savoir de quelle manière modifier vos souhaits en matière de cookies. Voici les pages d’aide pour :

[Internet Explorer](http://windows.microsoft.com/fr-FR/windows-vista/Block-or-allow-cookies) ™

[Safari](http://docs.info.apple.com/article.html?path=Safari/3.0/fr/9277.html) ™

[Chrome](http://support.google.com/chrome/bin/answer.py?hl=fr&hlrm=en&answer=95647) ™

[Firefox](https://support.mozilla.org/fr/kb/activer-desactiver-cookies) ™

[Opera](http://help.opera.com/Windows/10.20/fr/cookies.html) ™

**b) via les plateformes interprofessionnelles**

Vous pouvez vous connecter au site Youronlinechoices, proposé par les professionnels de la publicité digitale regroupés au sein de l'association européenne EDAA (European Digital Advertising Alliance) géré en France par l'Interactive Advertising Bureau France.

Cette plate-forme européenne est partagée par des centaines de professionnels de la publicité sur Internet et constitue une interface centralisée vous permettant d'exprimer votre refus ou votre acceptation des cookies  utilisés afin d'adapter les publicités à la navigation de votre terminal.

[http://www.youronlinechoices.com/fr/controler-ses-cookies/.](http://www.youronlinechoices.com/fr/controler-ses-cookies/)

Notez que cette procédure n'empêchera pas l'affichage de publicités sur les sites Internet que vous visitez. Elle ne bloquera que les technologies qui permettent d'adapter des publicités à vos centres d'intérêts.

**c) via les sites professionnels**

- Google Analytics : vous pouvez refuser les cookies de Google Analytics en cliquant sur ce lien ci-contre : [https://tools.google.com/dlpage/gaoptout](https://tools.google.com/dlpage/gaoptout)

- _Flash_ © de Adobe Flash Player : Dans la mesure où votre terminal serait susceptible de visualiser des contenus développés avec le langage Flash, nous vous invitons à accéder à vos outils de gestion des cookies Flash, directement depuis le site Web d'Adobe : [http://www.adobe.com/fr/](http://www.adobe.com/fr/). (Onglet Cookies)

- Réseaux sociaux : Pour les applications informatiques émanant de tiers, qui vous permettent de partager des contenus du Site Internet avec d'autres personnes ou de faire connaître à ces autres personnes votre opinion concernant un contenu du Site Internet, comme c’est notamment le cas des boutons "Partager", "J'aime", issus de réseaux sociaux tels que Facebook, "Google+","Twitter", etc.. Lorsque vous consultez une page du Site Internet contenant un tel bouton, votre navigateur établit une connexion directe avec les serveurs du réseau social. Si vous ne souhaitez pas que le réseau social relie les informations collectées par l'intermédiaire du Site Internet à votre compte utilisateur, vous devez vous déconnecter du réseau social avant de visiter le Site Internet. Nous vous invitons à consulter les politiques de protection de la vie privée de ces réseaux sociaux afin de prendre connaissance des finalités d'utilisation, notamment publicitaires, des informations de navigation qu'ils peuvent recueillir grâce à ces boutons applicatifs.

### **CHARTE D’UTILISATION DU SITE**

#### **1\. OBJET**

La présente Charte d’utilisation a pour objet de définir les conditions dans lesquelles les internautes peuvent accéder au site [www.novaplanet.com](http://www.novaplanet.com/)  (ci-après « le Site ») et utiliser les contenus et services proposés par le Site.

La navigation sur le Site implique l'acceptation pleine et entière de la présente charte d’utilisation. Elle peut être modifiée et/ou complétée à tout moment par Radio Nova. C’est la version en ligne à la date d’accès du Site qui sera applicable. Aussi, il est conseillé à l’internaute de consulter régulièrement la Charte d’utilisation du Site.

#### **2\. ACCES ET INSCRIPTION AU SITE**

L'accès au Site est possible 24H/ 24, 7J/7, sauf en cas de force majeure ou d'un événement hors du contrôle de Radio Nova et sous réserve des éventuelles pannes et interventions de maintenance nécessaires au bon fonctionnement du Site. 

Radio Nova se réserve le droit de modifier et/ou de supprimer à tout moment, sans préavis tout ou partie des contenus et services proposés sur le Site. 

L'accès au service se fait à partir d'un appareil connecté à un réseau de télécommunication permettant l'accès au Site selon les protocoles de communication utilisés sur le réseau d'Internet.

L’internaute qui souhaite bénéficier de tous les services du Site (comme commenter les vidéos, articles et podcasts, recevoir les newsletters, etc) est invité à s’inscrire sur le Site. Pour cela, il lui suffit de se rendre sur le lien « Connexion » puis de cliquer sur « **[Créer un compte](http://www.novaplanet.com/user/register)** » et de compléter le formulaire d’inscription au Site en renseignant les champs suivants : Pseudo / Email / Code postal / Mot de passe. L’internaute recevra un e-mail de confirmation de son inscription à l’adresse mail qu’il aura renseignée lors de son inscription et lui rappelant les identifiants (pseudo et mot de passe) choisis par ses soins qui lui permettront de se connecter au Site. Il sera invité à modifier son mot de passe en se rendant sur l’url [http://www.novaplanet.com/user/password](http://www.novaplanet.com/user/password).

Pour participer aux Jeux [**Nova Gratos**](http://www.novaplanet.com/categorie/tag-mot-clef/gratos), l’internaute devra par ailleurs compléter le formulaire avec ses Nom, Prénom, Adresse et numéro de téléphone. Ces données sont récoltées exclusivement aux fins d’organisation des Jeux (information et envoi des lots aux gagnants). Toute participation aux Jeux Nova Gratos implique l’acceptation pleine et entière des conditions du **[règlement des jeux Nova Gratos](http://www.novaplanet.com/events/Reglement-Nova-Gratos.pdf) **.

L’internaute est seul responsable de l’utilisation de ses identifiants. Tout accès au Site effectué à partir des identifiants d’un internaute sera réputé avoir été effectué par cet internaute. La sauvegarde de la confidentialité des identifiants de l’internaute relève de sa seule et entière responsabilité. En cas de perte, détournement ou utilisation non autorisée de ses identifiants, l’internaute est tenu d’en avertir immédiatement Radio Nova par e-mail à l’adresse [contact@novaplanet.com](mailto:contact@novaplanet.com).

L’internaute a la possibilité de se désinscrire à tout moment du Site en écrivant à l'adresse suivante : RADIO NOVA - Service Web - 127, avenue Ledru-Rollin  75011 Paris

ou bien via l'adresse [contact@novaplanet.com](mailto:contact@novaplanet.com).  

#### **3\. UTILISATION DU SERVICE PODCAST**

La société Radio Nova propose à ses internautes un service de podcast permettant le téléchargement de certains éléments du programme Radio Nova en vue de leur réécoute à la demande.

Pour utiliser ce service, vous devez préalablement installer un logiciel de podcast (iTunes, Juice,  ou d'autres logiciels gratuits sur Framasoft, etc.).

Les podcasts de Radio Nova sont accessibles à l’adresse url suivante : [http://www.novaplanet.com/radionova/podcasts](http://www.novaplanet.com/radionova/podcasts).

Si vous choisissez iTunes, dans l'émission Podcast qui vous intéresse, cliquez sur l'icône iTunes. L'ensemble des opérations de téléchargement et d'abonnement sont alors gérées de façon automatique par iTunes. Vous n'avez plus qu'à régler vos préférences Podcast dans le menu préférences > podcast.

Si vous préférez un autre logiciel de gestion de Podcast, cliquez sur l'icône RSS. Dans la fenêtre qui s'ouvre alors, copiez-collez l'adresse de la feuille RSS du Podcast de Radio Nova auquel vous souhaitez vous abonner puis suivre les indications de votre logiciel.

Les modalités d’accès aux podcasts restent de la seule responsabilité de chaque internaute utilisateur qui s’engage à en faire son affaire personnelle et à en supporter les coûts éventuels (frais de matériels et de connexion, frais de logiciels, etc.) ; la responsabilité de Radio Nova  ne pouvant être recherchée à cet égard.

Dans le cadre du service podcast, Radio Nova accorde, à titre gracieux, aux internautes une licence d’utilisation, révocable et non-exclusive, strictement personnelle et incessible de:

- reproduire par téléchargement les contenus proposés dans le cadre du service de podcast exclusivement sur leur ordinateur et/ou baladeur numérique et ce pour une utilisation strictement personnelle, privée et non- commerciale;

- représenter les contenus proposés dans le cadre du service de podcast sur leur ordinateur et/ou baladeur numérique à des fins strictement personnelles, privées et non-commerciales.

Radio Nova reste seul titulaire des droits attachés à ses programmes radiophoniques et à ses podcasts conformément aux lois nationales et internationales relatives à la propriété intellectuelle. Toute autre utilisation des podcasts autre que celles susvisées est interdite.

#### **4\. PROPRIETE INTELLECTUELLE / DROITS CONCEDES AUX INTERNAUTES**

Radio Nova et/ou ses partenaires sont les titulaires exclusifs de tous les droits de propriété intellectuelle portant tant sur la structure que sur le contenu du Site et ce dans le monde entier.

Sauf  autorisation préalable et expresse de Radio Nova, l'internaute s'interdit de reproduire et/ou d'utiliser les marques et logos présents sur le Site, ainsi que de modifier, copier, traduire, reproduire, vendre, publier, exploiter et diffuser dans un format numérique  ou autre, tout ou partie des informations, textes, photos, images, vidéos et données présentes sur le Site. La violation de ces dispositions impératives soumet le contrevenant, et toutes personnes responsables, aux peines pénales et civiles prévues par la loi. 

#### **5\. LIMITATION DE RESPONSABILITE**

Radio Nova  met tout en œuvre pour s'assurer de la fiabilité des informations accessibles par l'intermédiaire du Site. Toutefois, elle ne garantit en aucune manière que ces informations sont exactes, complètes et à jour et de manière générale, ne fournit aucune garantie expresse ou tacite, concernant tout ou partie de son Site.

Radio Nova fournit les contenus (tels que notamment images, sons, vidéos, photographies, logos, marques, éléments graphiques, outils, logiciels, documents et toutes autres données) sans garantie d'aucune sorte. L'utilisation du Site se fait aux risques et périls de l’internaute.

Le téléchargement de tous contenus de quelque sorte que ce soit et/ou de logiciels (widget, utilitaires ou autres) lors de l'utilisation du Site se fait aux risques et périls de l’internaute. L’internaute reste seul responsable pour tout dommage subi par son ordinateur ou autre terminal de réception notamment mobile et/ou de toutes pertes de données consécutives à ce téléchargement.

**Liens hypertextes**

Le Site comporte des informations mises à disposition par des sociétés externes ou des liens hypertextes vers d'autres sites qui n'ont pas été développés par Radio Nova. L'existence d'un lien du Site novaplanet.com vers un autre site ne constitue pas une validation de ce site ou de son contenu. La responsabilité de Radio Nova ne saurait être engagée du fait aux informations, opinions et recommandations formulées par des tiers.

**Commentaires**

Radio Nova exclut toute responsabilité en ce qui concerne la validité ou le contenu des commentaires publiés par les internautes. De manière générale, tout message publié sur le Site demeure sous l'entière responsabilité de son auteur. 

A cet égard, l'internaute s'engage à ne pas contrevenir à la réglementation et aux usages en vigueur ainsi qu’aux intérêts légitimes des tiers et de Radio Nova.

L’internaute s’engage ainsi à ne pas diffuser :

- de message ou d’information illicite et notamment qui seraient contraire à l’ordre public et aux bonnes mœurs (notamment à caractère injurieux, diffamatoire, dénigrant, raciste, xénophobe, révisionniste, violent ou pornographique ou pédophile, et/ou susceptible par leur nature de porter atteinte au respect de la personne humaine et de sa dignité, à l'égalité entre les femmes et les hommes et à la protection des enfants et des adolescents, etc.).

- de messages portant atteinte au droit d’auteurs et droits voisins, droit à l’image des personnes et des biens.

- de messages comportant des coordonnées personnelles (nom, prénom, adresses postales, mail, numéros de téléphones…) sur le Site.

- des messages de nature promotionnelle et/ou commerciale.

L’internaute est informé que les messages qu’il diffuse sont susceptibles d’être contrôlés par un modérateur. Radio Nova se réserve le droit de supprimer tout message qui violerait les présentes, et de suspendre ou supprimer le compte utilisateur de l’internaute, sans préjudice du droit d’exercer toute poursuite judiciaire.

#### **6\. LOI APPLICABLE**

La présente Charte d’application est régie par le droit français.

EN CAS DE LITIGE DÉCOULANT DE L’INTERPRÉTATION OU DE L’EXÉCUTION DE LA PRÉSENTE CHARTE D’UTILISATION, LES PARTIES S’ENGAGENT À RECHERCHER UNE SOLUTION AMIABLE. 

À DÉFAUT D’UNE TELLE SOLUTION, LE DIFFÉREND SERA DE LA COMPÉTENCE EXCLUSIVE DES TRIBUNAUX FRANCAIS, QU’IL Y AIT OU NON PLURALITÉ DE DÉFENDEURS OU APPEL EN GARANTIE.