#! /bin/bash
shopt -s expand_aliases

source ~/Config/sshpass.zsh

REPO="/var/www/novatube"
LAST_BKP="2017-11-27"

# sshp_perso rsync -azP ./bkp/$LAST_BKP perso:$REPO/bkp
sshp_perso ssh perso "cd $REPO && docker-compose -f docker-compose.yml -f docker-compose.backup.yml run --rm backup mongorestore --db novatube --host db /novatube/bkp/$LAST_BKP/novatube"
