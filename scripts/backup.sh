#! /bin/bash
shopt -s expand_aliases

source ~/Config/sshpass.zsh

DATE=`date +%Y-%m-%d`
REPO="/var/www/novatube"
FOLDER="bkp/$DATE"

mkdir -p $FOLDER

sshp_perso ssh perso "cd $REPO && docker-compose -f docker-compose.yml -f docker-compose.backup.yml run --rm backup mongodump --out /novatube/$FOLDER --db novatube --host db"
sshp_perso rsync -azP perso:$REPO/bkp/ ./bkp
sshp_perso ssh perso "rm -rf $REPO/bkp/*"