#! /bin/bash

JS=$(cat <<-END
require('./server/config/setup');
Track = require('./server/models/Track');
Track.removeYoutubeIds().then(r => console.log('Cleaned', r));
process.exit();
END
)

nodejs -e "$JS"