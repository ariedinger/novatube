require('../utils/time');
var Track = require('../models/Track');
var _ = require('lodash');
var mongoose = require('mongoose');
var secrets = require('../config/secrets');

mongoose.connect(secrets.db, function(err) {
  if (err) {
    console.log(err);
  }

  Track.find({
    youtubeID: {'$ne': null }
  }).select('youtubeID').exec(function(err, youtubeIDsArray){
    console.log(youtubeIDsArray.length + ' tracks');
    var uniqIds = _.uniq(_.map(youtubeIDsArray, function(track){ return track.youtubeID}));
    console.log(uniqIds.length + ' ids');
    uniqIds.forEach(function(youtubeID){
      Track.find({youtubeID: youtubeID}).exec(function(err, tracks) {
        if (tracks.length > 1) {
          console.log(tracks.length + ' found')

          var cpt = 0;

          if (!tracks[cpt].artist || !tracks[cpt].title || !tracks[cpt].youtubeID || !tracks[cpt].cover) {
            cpt += 1;
            if (cpt >= tracks.length) {
              return console.log('Nothing cool')
            }
          }

          Track.create({
            artist: tracks[cpt].artist,
            title: tracks[cpt].title,
            youtubeID: tracks[cpt].youtubeID,
            cover: tracks[cpt].cover,
            playedAtTimestamp: Math.max.apply(null, _.compact(_.map(tracks, function(track) {return parseInt(track.playedAtTimestamp)}))),
            listenCount: tracks.length
          }, function(err, newTrack){
            console.log(newTrack.toString());

            tracks.forEach(function(track) {
              track.remove();
            });

            Track.find({youtubeID: youtubeID}).exec(function(err, tracks) {
              console.log('Now ' + tracks.length + ' found');
            });
          });


        } else {
          if (tracks[0]) {
            tracks[0].incCounter().save();
            console.log('Only 1 track, listenCount:', tracks[0].listenCount);
          } else {
            console.log('weird');
          }
        }
      });
    });
  });

});

// process.stdin.resume();

// 1424426062000,