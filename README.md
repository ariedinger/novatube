![Novatube](/public/images/novatube.png?raw=true "Novatube")

NovaTube
========

> Continuous Nova playlist based on last played tracks played with Youtube.

## Installation

1. Duplicate `.env.example` to `.env` and get a [Youtube API Key](https://console.developers.google.com)

### Locally

2. Install dependencies: `npm install`
3. Start Hacking: `npm run dev`

### With docker

2. `docker-compose build`
3. `docker-compose up`

## TODOs

- [ ] Permanently fetch real time radios (radio sympa)
- [ ] Store refresh time per crawler
- [ ] Plug user + social actions
- [ ] Add CORS security
- [ ] Add Server side rendering?
- [ ] Store failed searches
- [ ] Add a contact link